<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

  if (Auth::guest()) {
    $query=DB::table('dbo.ViewTST_ActualDepotSerie')->get();
    dd($query);
    return view('welcome');
  }
  return redirect('home');
});

Route::resource('pedidorequerido', ViewTSTPedidoRequeridosController::class);
Route::resource('detalleDePedido', 'DetalleDePedidoController');
Route::resource('ordenesProduccion', 'OrdenesProduccionController');
Route::resource('usermarlik','UserMarlikController');
Route::get('api/index','OrdenesProduccionController@apiIndex');
Route::post('insumos', 'OrdenesProduccionController@insumoIndex');
Route::get('/home', 'HomeController@index')->name('home');
Route::resource('serialproductos', 'SerialProductController');
Route::get('api/serial', 'SerialProductController@indexApp');
Route::post('api/programer', 'OrdenesProduccionController@programacion');
Route::get('api/obtenerultimo', 'OrdenesProduccionController@ultimoProgramado');
Route::post('api/programarseriales', 'OrdenesProduccionController@programarSeriales');
Route::get('llenar', 'OrdenesProduccionController@llenar');
Route::get('prodct/series/{folio}', 'OrdenesProduccionController@detallePedidos');
Auth::routes();

Route::group(['middleware' => 'auth'], function () {

  Route::resource('user', 'UserController', ['except' => ['show']]);

  Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
  Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
	Route::put('profile/company', ['as' => 'profile.company', 'uses' => 'ProfileController@company']);

  Route::get('{page}', ['as' => 'page.index', 'uses' => 'PageController@index']);
});
