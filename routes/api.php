<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api'], function()
{
  Route::get('employees/{company_id}', '\App\Http\Controllers\APIController@getEmployees');
  Route::get('companies', '\App\Http\Controllers\APIController@getCompanies');
  Route::get('admin/users', '\App\Http\Controllers\APIController@getAdminUsers');
  Route::get('products/{company_id}', '\App\Http\Controllers\APIController@getProducts');
  Route::get('search/products/{company_id}/{busqueda}', '\App\Http\Controllers\APIController@getProductsSearch');
  Route::get('search/employees/{company_id}/{busqueda}', '\App\Http\Controllers\APIController@getEmployeesSearch');
  Route::get('search/companies/{busqueda}', '\App\Http\Controllers\APIController@getCompaniesSearch');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
