@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'dashboard'
])

@section('content')
<div class="content" id="vue-app">
    <div class="card shadow col-md-12">

        <div class="card-header border-0">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <h3 class="mb-0">PEDIDOS</h3><br>
                </div>
        
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <button type="button" class="btn btn-primary" @click="checarInsumos"><i class="nc-icon"><img src="{{ asset('paper/img/supply.png') }}"></i>
Insumos</button>
                            
                        </div>
                    </div>
                
               <!-- <div class="col-md-2 text-right">
                    <button type="button" class="btn btn-secondary col-md-12" data-toggle="modal" data-target="#modalInmobiliaria">
                        nueva inmobiliaria
                    </button>
                </div>-->
            </div>
        </div>

        <div class=" mt-3">
                        <table class="table table-bordered table-striped align-middle" >
                            <thead class="thead-dark">
                                <tr>
                                    <th><input type="checkbox" v-model="select_all" @click="select"></th>
                                    <th>Pedido</th>
                                    <th>Folio</th>
                                    <th>Clave</th>
                                    <th>Descripción</th>
                                    <th>Cantidad</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="pedido in pedidos">
                                    <td><input type="checkbox" :value="pedido.productID" v-model="selected"></td>
                                    <td>@{{pedido.orden}}</td>
                                    <td>@{{pedido.folio}}</td>
                                    <td>@{{pedido.clave}}</td>
                                    <td>@{{pedido.descripcion}}</td>
                                    <td>@{{pedido.cantidad}}</td>
                                </tr>
                            </tbody>
                        </table>
        </div>
    </div>
    
    <div class="modal fade" id="modalAgAs" tabindex="-1" aria-labelledby="modalAgAsLabel" aria-hidden="true">
        <div class="modal-dialog" style="min-width: 40%;">
            <div class="modal-content" >
                <div class="modal-header">
                    <h5 class="modal-title" id="modalAgAsLabel">Agentes</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        
                    </div>
                </div>
                <div class="modal-footer">
                
                </div>
            </div>
        </div>
    </div>

    <!-- Modal nueva -->
    <div class="modal fade" id="modalInsumos" tabindex="-1" aria-labelledby="modalInsumos" aria-hidden="true" style="width: 90% !important;">
        <div class="modal-dialog modal-lg" style="min-width: 70%; font-size: 10px;">
            <div class="modal-content" >
                <div class="modal-header">
                        <div class="row align-items-center">
                            <div class="col-md-7">
                                <h3 class="mb-0">REQUERIDO</h3><br>
                            </div>
                            <div class="input-group no-border col-md-3"><i class="fa-solid fa-wrench-simple"></i>
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <button type="button" class="btn btn-primary" @click="verDetalles"><i class="nc-icon nc-settings-gear-65"></i>Detalles</button>
                                        
                                    </div>
                                </div>
                            </div>
                           <!-- <div class="col-md-2 text-right">
                                <button type="button" class="btn btn-secondary col-md-12" data-toggle="modal" data-target="#modalInmobiliaria">
                                    nueva inmobiliaria
                                </button>
                            </div>-->
                        </div>
                </div>
                <div class="modal-body">
                    <div class="row ">
                       <table class="table table-bordered table-striped align-middle" >
                            <thead class="thead-dark" style="font-size:12px !important;">
                                <tr>
                                    <th>ProductKey</th>
                                    <th>Product Name</th>
                                    <th>Requerido</th>
                                    <th>Presente</th>
                                    <th>Faltante</th> 
                                    <th>Unidad</th>
                                    <th>Precio</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="insumo in insumos">
                                    <td>@{{insumo.ProductKey}}</td>
                                    <td>@{{insumo.ProductName}}</td>
                                    <td>@{{insumo.QtyMinimum}}</td>
                                    <td>@{{insumo.CantPresente}}</td>
                                    <td>@{{insumo.CantDisponible}}</td>
                                    <td>Kg</td>
                                    <td>0</td>
                                </tr>
                            </tbody>
                        </table>

                    </div>   
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button">Guardar</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalDetalle" tabindex="-1" aria-labelledby="modalDetalle" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="min-width: 90%;">
            <div class="modal-content" id="detalleContent" >
                <div class="modal-header">
                    <h5 class="modal-title" id="modalInmobiliariaLabel">INSUMOS REQUERIDOS</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row ">
                       <table class="table table-striped table-bordered table-responsive align-middle" >
                            <thead class="thead-dark" style="font-size:12px !important;">
                                <tr>
                                    <th scope="col">ProductID</th>
                                    <th scope="col">Clave</th>
                                    <th scope="col">Producto</th>
                                    <th scope="col">Unidad</th>
                                    <th scope="col">Costo</th> 
                                    <th scope="col">CantMax</th>
                                    <th scope="col">CantCom</th>
                                    <th scope="col">Requerido</th>
                                    <th scope="col">Presente</th>
                                    <th scope="col">Saldo</th>
                                    <th scope="col">CantXRecibir</th>
                                    <th scope="col">D. entrega</th> 
                                    <th scope="col">Saldo</th>
                                    <th scope="col">StandarPack</th>
                                    <th scope="col">SugCompra</th>
                                    <th scope="col">Unidad</th>
                                    <th scope="col">Semanas</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="detal in insumosDetalle">
                                    <td>@{{detal.ProductID}}</td>
                                    <td>@{{detal.Clave}}</td>
                                    <td>@{{detal.Producto}}</td>
                                    <td>@{{detal.Unidad}}</td>
                                    <td>@{{detal.Costo}}</td>
                                    <td>@{{detal.CantMax}}</td>
                                    <td>@{{detal.CantCom}}</td>
                                    <td>@{{detal.Requerido}}</td>
                                    <td>@{{detal.Presente}}</td>
                                    <td>@{{detal.Saldo}}</td>
                                    <td>@{{detal.CantXRecibir}}</td>
                                    <td>@{{detal.entrega}}</td>
                                    <td>@{{detal.SaldoCant}}</td>
                                    <td>@{{detal.StandarPack}}</td>
                                    <td>@{{detal.SugCompra}}</td>
                                    <td>@{{detal.Unidad}}</td>
                                    <td>@{{detal.Semanas}}</td>
                                </tr>
                            </tbody>
                        </table>

                    </div>   
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button">Exportar Excel</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        var app = new Vue({
        el: '#vue-app',
        data: {
            buscar: '',
            aux:0,
            pedidos:[],
            selected:[],
            insumos:[],
            select_all:false,
            insumosDetalle:[
                {
                    ProductID:"7125",
                    Clave:"12100119",
                    Producto:"ALUMINA OE 431",
                    Unidad:"KG",
                    Costo:"35.55",
                    CantMax:"238.00",
                    CantCom:"0.1320",
                    Requerido:"31.4160",
                    Presente:"2761.44",
                    Saldo:"2730.0240",
                    CantXRecibir:"0.0",
                    entrega:"22",
                    SaldoCant:"27.30",
                    StandarPack:"50.00",
                    SugCompra:"20.3713",
                    Unidad:"LBS",
                    Semanas:"5"
                },
                {
                    ProductID:"7147",
                    Clave:"12100119",
                    Producto:"GRANITO DE ALUM. FROST L2063DL/G",
                    Unidad:"KG",
                    Costo:"35.55",
                    CantMax:"238.00",
                    CantCom:"0.1320",
                    Requerido:"31.4160",
                    Presente:"2761.44",
                    Saldo:"2730.0240",
                    CantXRecibir:"0.0",
                    entrega:"22",
                    Saldo:"27.30",
                    StandarPack:"50.00",
                    SugCompra:"20.3713",
                    Unidad:"LBS",
                    Semanas:"5"
                },
                {
                    ProductID:"7125",
                    Clave:"12100119",
                    Producto:"PIGMENTO AMARILLO CQ-47982",
                    Unidad:"KG",
                    Costo:"35.55",
                    CantMax:"238.00",
                    CantCom:"0.1320",
                    Requerido:"31.4160",
                    Presente:"2761.44",
                    Saldo:"2730.0240",
                    CantXRecibir:"0.0",
                    entrega:"22",
                    Saldo:"27.30",
                    StandarPack:"50.00",
                    SugCompra:"20.3713",
                    Unidad:"LBS",
                    Semanas:"5"
                },
                {
                    ProductID:"7125",
                    Clave:"12100119",
                    Producto:"ALUMINA OE 431",
                    Unidad:"KG",
                    Costo:"35.55",
                    CantMax:"238.00",
                    CantCom:"0.1320",
                    Requerido:"31.4160",
                    Presente:"2761.44",
                    Saldo:"2730.0240",
                    CantXRecibir:"0.0",
                    entrega:"22",
                    Saldo:"27.30",
                    StandarPack:"50.00",
                    SugCompra:"20.3713",
                    Unidad:"LBS",
                    Semanas:"5"
                },
            ]
        },
        
        methods:{ 
            getData: function(){
                let vue=this;
                axios.get('/api/index').then(function(response){
                    vue.pedidos=response.data.pedido
                })
            },
            select(){
                this.selected = [];
                console.log(this.pedidos.length)
                if(!this.select_all){
                    this.aux=1

                    for(let i in this.pedidos)
                        this.selected.push(this.pedidos[i].productID)
                }
            },

            checarInsumos: function(){
                console.log(this.selected)
                let vue =this;
                axios.post('insumos',{ productos:vue.selected}).then(response=>{
                    vue.insumos=response.data.insumos
                    console.log(vue.insumos)
                })
                $('#modalInsumos').modal();
            },

            verDetalles: function(){

                $('#modalDetalle').modal();
            }

        },

        created: function(){
            let vue=this;
            vue.getData();
        },

       /* mounted: function(){
            let t = this;
            this.$nextTick(function () {
                t.getData();
        });
      }*/
                
    })
    </script>
@endpush