@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'user'
])

@section('content')
  <style media="screen">
    table th, td{
      text-align: center !important;
    }
  </style>
  <div class="content" id="app">
    @if (session('message'))
        <div class="alert alert-success" role="alert">
            {{ session('message') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger" role="alert">
            {{ session('error') }}
        </div>
    @endif

      <div class="card shadow col-md-12">
          <div class="card-header border-0">
            <div class="row align-items-center">
                <div class="col-md-10">
                    <h3 class="mb-0">USUARIOS PANEL</h3><br>
                </div>
                {{-- <div class="input-group no-border col-md-3">
                    <input type="text" value="" class="form-control" placeholder="Buscar..." v-model="buscar" @input="busqueda">
                    <div class="input-group-append">
                          <div class="input-group-text">
                              <i class="nc-icon nc-zoom-split"></i>
                          </div>
                    </div>
                </div> --}}
                <div class="col-md-2 text-right">
                    <a href="/user/create" class="btn  btn-primary col-md-12 btn-round"> NUEVO USUARIO</a>
                </div>
            </div>
          </div>
          <div class="table-responsive">
              <table class="table align-items-center table-flush">
                  <thead class="thead-light">
                      <tr>
                          <th scope="col">Nombre</th>
                          <th scope="col">Email</th>
                          <th scope="col">Telefono</th>
                          <th scope="col">Celular</th>
                          <th scope="col"></th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr v-for="u in users">
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;@{{u.name}}</td>
                        <td>@{{u.email}}</td>
                        <td>@{{u.phone ? u.phone : 'N/A'}}</td>
                        <td>@{{u.cellphone ? u.cellphone : 'N/A'}}</td>
                        <td>
                          <div class="form-inline center" >
                            <a :href="'/user/' + u.id + '/edit'" class="btn btn-sm btn-info btn-round"><i class="fa fa-pencil"></i> Editar</a>
                            <form :action="'/user/' + u.id" method="POST" @submit="eliminar">
                              @method('DELETE') @csrf
                              <button type="submit" class="button btn btn-sm btn-danger btn-round "><i class="fa fa-trash" ></i> Eliminar</button>
                            </form>
                          </div>
                        </td>
                      </tr>
                  </tbody>
              </table>
          </div>
          <div class="card-footer py-4">
              <nav class="d-flex justify-content-end" aria-label="...">

              </nav>
          </div>
      </div>
  </div>
@endsection

@section('vuescripts')

  <script type="text/javascript">
    var app = new Vue({
      el: '#app',
      data: {
        buscar: '',
        users: [],
      },
      methods:{
        eliminar: function(e){
          e.preventDefault();
          swal({
            title: "¿Eliminar?",
            text: "Una ves eliminado, no podra deshacer esa accion.",
            icon: "warning",
            buttons: ['Cancelar', 'Eliminar'],
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              $(e.target).submit();
            }
          });
        },
        busqueda: function(){

        },
        getUsuarios: function(){
          let t = this;
          axios.get('/api/admin/users')
          .then(function (response) {
            t.users = response.data.users;
          })
          .catch(function (error) {
            console.log(error);
          });
        }
      },
      mounted: function () {
        let t = this;
        this.$nextTick(function () {
          t.getUsuarios();
        });
      }
    })
  </script>

@endsection
