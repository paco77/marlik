@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'empresas'
])

@section('content')
    <style media="screen">
        table td, th{
          text-align: center !important;
        }
        .notalign{
          text-align: left !important;
          padding-left: 120px !important
        }
    </style>
    <div class="content" id="vue-app">

      <div class="card shadow">
        @if($modo == 1)
          <form action="/user" method="post" enctype="multipart/form-data" @submit="validarFormulario">
        @else
          <form action="/user/{{$user->id}}" method="post" enctype="multipart/form-data" @submit="validarFormulario">
          @method('PUT')
        @endif
          @csrf
          <div class="card-header border-0">
              <div class="row align-items-center">
                  <div class="col-md-10">
                    @if($modo == 1)<h3 class="mb-0">REGISTRAR USUARIO</h3> @else <h3 class="mb-0">EDITAR USUARIO</h3> @endif
                    <br>
                  </div>
                  <div class="col-md-2 col-sm-12 text-right">
                    <a href="/user" class="btn btn-warning col-12 btn-round"><i class="fa fa-arrow-left"> </i> REGRESAR</a>
                  </div>
              </div>
              <hr>
              <div class="row card-body">
                <h5>Información general </h5>
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-6 col-sm-12 form-group">
                      <label for="">Nombre *</label>
                      <input type="text" class="form-control" name="nombre_usuario" value="{!!$user->name!!}" required>
                    </div>
                    <div class="col-md-6 col-sm-12 form-group">
                      <label for="">Email *</label>
                      <input type="text" class="form-control" name="email" value="{!!$user->email!!}" required>
                    </div>
                    <div class="col-md-3 col-sm-12 form-group">
                      <label for="">Telefono</label>
                      <input type="text" class="form-control" name="telefono_usuario" value="{!!$user->phone!!}">
                    </div>
                    <div class="col-md-3 col-sm-12 form-group">
                      <label for="">Celular</label>
                      <input type="text" class="form-control" name="celular" value="{!!$user->cellphone!!}">
                    </div>
                    <div class="col-md-3 col-sm-12 form-group">
                      <label for="">@if($modo == 1) Contraseña  @else Cambiar contraseña (opcional) @endif </label>
                      <input type="password" class="form-control" name="password" v-model="password">
                    </div>
                    <div class="col-md-3 col-sm-12 form-group">
                      <label for="">Repetir contraseña @if($modo != 1) (opcional) @endif</label>
                      <input type="password" class="form-control" v-model="password2" >
                    </div>

                  </div>
                </div>
                <div class="col-md-12"><br>
                  <h5>Permisos </h5>
                  <div class="row fluid-container">
                    <table class="table table-striped">
                      <thead>
                        <tr>
                          <th>permiso</th>
                          <th>Consultar</th>
                          <th>Alta</th>
                          <th>Baja</th>
                          <th>Modificacion</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>Empresas</td>
                          <td>
                            <div class="form-group">
                              <div class="form-check">
                                <label class="form-check-label" style="font-size: 10px">
                                  <input type="checkbox" class="form-check-input" name="permisos[]" value="c_empresas" v-model="c_empresas">
                                  <span class="form-check-sign"></span>
                                </label>
                              </div>
                            </div>
                          </td>
                          <td>
                            <div class="form-group">
                              <div class="form-check">
                                <label class="form-check-label" style="font-size: 10px">
                                  <input type="checkbox" class="form-check-input" name="permisos[]" value="a_empresas" v-model="a_empresas">
                                  <span class="form-check-sign"></span>
                                </label>
                              </div>
                            </div>
                          </td>
                          <td>
                            <div class="form-group">
                              <div class="form-check">
                                <label class="form-check-label" style="font-size: 10px">
                                  <input type="checkbox" class="form-check-input" name="permisos[]" value="b_empresas" v-model="b_empresas">
                                  <span class="form-check-sign"></span>
                                </label>
                              </div>
                            </div>
                          </td>
                          <td>
                            <div class="form-group">
                              <div class="form-check">
                                <label class="form-check-label" style="font-size: 10px">
                                  <input type="checkbox" class="form-check-input" name="permisos[]" value="m_empresas" v-model="m_empresas">
                                  <span class="form-check-sign"></span>
                                </label>
                              </div>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>Usuarios panel</td>
                          <td>
                            <div class="form-group">
                              <div class="form-check">
                                <label class="form-check-label" style="font-size: 10px">
                                  <input type="checkbox" class="form-check-input" name="permisos[]" value="c_usuarios" v-model="c_usuarios">
                                  <span class="form-check-sign"></span>
                                </label>
                              </div>
                            </div>
                          </td>
                          <td>
                            <div class="form-group">
                              <div class="form-check">
                                <label class="form-check-label" style="font-size: 10px">
                                  <input type="checkbox" class="form-check-input" name="permisos[]" value="a_usuarios" v-model="a_usuarios">
                                  <span class="form-check-sign"></span>
                                </label>
                              </div>
                            </div>
                          </td>
                          <td>
                            <div class="form-group">
                              <div class="form-check">
                                <label class="form-check-label" style="font-size: 10px">
                                  <input type="checkbox" class="form-check-input" name="permisos[]" value="b_usuarios" v-model="b_usuarios">
                                  <span class="form-check-sign"></span>
                                </label>
                              </div>
                            </div>
                          </td>
                          <td>
                            <div class="form-group">
                              <div class="form-check">
                                <label class="form-check-label" style="font-size: 10px">
                                  <input type="checkbox" class="form-check-input" name="permisos[]" value="m_usuarios" v-model="m_usuarios">
                                  <span class="form-check-sign"></span>
                                </label>
                              </div>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <th>BASE DE DATOS</th>
                          <th colspan="4"> </th>
                        </tr>
                        <tr>
                          <td>Objeciones</td>
                          <td>
                            <div class="form-group">
                              <div class="form-check">
                                <label class="form-check-label" style="font-size: 10px">
                                  <input type="checkbox" class="form-check-input" name="permisos[]" value="c_objeciones" v-model="c_objeciones">
                                  <span class="form-check-sign"></span>
                                </label>
                              </div>
                            </div>
                          </td>
                          <td>
                            <div class="form-group">
                              <div class="form-check">
                                <label class="form-check-label" style="font-size: 10px">
                                  <input type="checkbox" class="form-check-input" name="permisos[]" value="a_objeciones" v-model="a_objeciones">
                                  <span class="form-check-sign"></span>
                                </label>
                              </div>
                            </div>
                          </td>
                          <td>
                            <div class="form-group">
                              <div class="form-check">
                                <label class="form-check-label" style="font-size: 10px">
                                  <input type="checkbox" class="form-check-input" name="permisos[]" value="b_objeciones" v-model="b_objeciones">
                                  <span class="form-check-sign"></span>
                                </label>
                              </div>
                            </div>
                          </td>
                          <td>
                            <div class="form-group">
                              <div class="form-check">
                                <label class="form-check-label" style="font-size: 10px">
                                  <input type="checkbox" class="form-check-input" name="permisos[]" value="m_objeciones" v-model="m_objeciones">
                                  <span class="form-check-sign"></span>
                                </label>
                              </div>
                            </div>
                          </td>

                        </tr>
                        <tr>
                          <td>Recomendaciones</td>
                          <td>
                            <div class="form-group">
                              <div class="form-check">
                                <label class="form-check-label" style="font-size: 10px">
                                  <input type="checkbox" class="form-check-input" name="permisos[]" value="c_recomendaciones" v-model="c_recomendaciones">
                                  <span class="form-check-sign"></span>
                                </label>
                              </div>
                            </div>
                          </td>
                          <td>
                            <div class="form-group">
                              <div class="form-check">
                                <label class="form-check-label" style="font-size: 10px">
                                  <input type="checkbox" class="form-check-input" name="permisos[]" value="a_recomendaciones" v-model="a_recomendaciones">
                                  <span class="form-check-sign"></span>
                                </label>
                              </div>
                            </div>
                          </td>
                          <td>
                            <div class="form-group">
                              <div class="form-check">
                                <label class="form-check-label" style="font-size: 10px">
                                  <input type="checkbox" class="form-check-input" name="permisos[]" value="b_recomendaciones" v-model="b_recomendaciones">
                                  <span class="form-check-sign"></span>
                                </label>
                              </div>
                            </div>
                          </td>
                          <td>
                            <div class="form-group">
                              <div class="form-check">
                                <label class="form-check-label" style="font-size: 10px">
                                  <input type="checkbox" class="form-check-input" name="permisos[]" value="m_recomendaciones" v-model="m_recomendaciones">
                                  <span class="form-check-sign"></span>
                                </label>
                              </div>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <div class="col-md-12 col-sm-12 text-right"><br><br>
                  <button type="submit" class="btn btn-success col-md-2 btn-round">Guardar</button>
                </div>
              </div>
          </div>
        </form>
      </div>
    </div>
@endsection

@section('vuescripts')
  <script type="text/javascript">
      var app = new Vue({
        el: '#vue-app',
        data: {
          c_empresas: false,
          a_empresas: false,
          b_empresas: false,
          m_empresas: false,
          c_usuarios: false,
          a_usuarios: false,
          b_usuarios: false,
          m_usuarios: false,
          c_objeciones: false,
          a_objeciones: false,
          b_objeciones: false,
          m_objeciones: false,
          c_recomendaciones: false,
          a_recomendaciones: false,
          b_recomendaciones: false,
          m_recomendaciones: false,
          password: '',
          password2: '',
        },
        methods:{
          existe: function(variable){
            if(typeof(variable) != 'undefined')
              return true;
            else
              return false;
          },
          validarFormulario: function(e){

            @if($modo == 1)
              if(this.password == this.password2 && this.password.length >= 6)
                return true;
            @else
              if(this.password == '' || (this.password == this.password2 && this.password.length >= 6))
                return true;
            @endif


            e.preventDefault();
            swal('Lo sentimos!', 'La contraseñas deben ser iguales y tener al menos 6 caracteres.', 'info');
            return;

          }
        },
        mounted: function () {
          this.$nextTick(function () {
            let t = this;
            @if($modo != 1)
            t.c_empresas = t.existe({{$user->hasPermissionTo('c_empresas')}});
            t.a_empresas = t.existe({{$user->hasPermissionTo('a_empresas')}});
            t.b_empresas = t.existe({{$user->hasPermissionTo('b_empresas')}});
            t.m_empresas = t.existe({{$user->hasPermissionTo('m_empresas')}});
            t.c_usuarios = t.existe({{$user->hasPermissionTo('c_usuarios')}});
            t.a_usuarios = t.existe({{$user->hasPermissionTo('a_usuarios')}});
            t.b_usuarios = t.existe({{$user->hasPermissionTo('b_usuarios')}});
            t.m_usuarios = t.existe({{$user->hasPermissionTo('m_usuarios')}});
            t.c_objeciones = t.existe({{$user->hasPermissionTo('c_objeciones')}});
            t.a_objeciones = t.existe({{$user->hasPermissionTo('a_objeciones')}});
            t.b_objeciones = t.existe({{$user->hasPermissionTo('b_objeciones')}});
            t.m_objeciones = t.existe({{$user->hasPermissionTo('m_objeciones')}});
            t.c_recomendaciones = t.existe({{$user->hasPermissionTo('c_recomendaciones')}});
            t.a_recomendaciones = t.existe({{$user->hasPermissionTo('a_recomendaciones')}});
            t.b_recomendaciones = t.existe({{$user->hasPermissionTo('b_recomendaciones')}});
            t.m_recomendaciones = t.existe({{$user->hasPermissionTo('m_recomendaciones')}});
            @endif

          });
        }
      });

  </script>
@endsection
