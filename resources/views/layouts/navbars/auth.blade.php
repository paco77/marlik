<style media="screen">
  .sidebar-mini-icon{
      font-size: 17px !important;
      padding-top: 4px
  }


</style>
<div class="sidebar" data-color="white" data-active-color="danger">
    <div class="logo">
        <a href="" class="simple-text logo-mini">
            <div class="logo-image-small">
                <img src="{{ asset('paper') }}/img/logo-small.png">
            </div>
        </a>
        <a href="/home" class="simple-text logo-normal">
            &nbsp;&nbsp;{{ __('MARLIK LOGO') }}
        </a>
    </div>
    <div class="sidebar-wrapper">

      <ul class="nav">
        <li>
          <a href="/ordenesProduccion">
            <i class="nc-icon"><img src="{{ asset('paper/img/productos.svg') }}"></i>
            <p>{{ __('PEDIDOS') }}</p>
          </a>
        </li>
       <li>
          <a href="/serialproductos">
            <i class="nc-icon"><img src="{{ asset('paper/img/connections.png') }}"></i>
            <p>{{ __('TRANSPASOS') }}</p>
          </a>
        </li>
        <li>
          <a href="/usuarsmarlik">
            <i class="nc-icon"><img src="{{ asset('paper/img/connections.png') }}"></i>
            <p>{{ __('USUARIOS') }}</p>
          </a>
        </li>
        <!--<li>
          <a href="/">
            <i class="nc-icon"><img src="{{ asset('paper/img/productos.svg') }}"></i>
            <p>{{ __('PRODUCTOS') }}</p>
          </a>
        </li>
        <hr style="background-color: white !important; opacity: .2; margin-left: 20px; margin-right: 20px">
        <li>
          <a href="/user">
            <i class="nc-icon"><img src="{{ asset('paper/img/usuarios.svg') }}"></i>
            <p>{{ __('USUARIOS') }}</p>
          </a>
        </li>
        <li>
          <a data-toggle="collapse" aria-expanded="false" href="#catalogo">
            <i class="nc-icon"><img src="{{ asset('paper/img/catalogo.svg') }}"></i>
            <p>
              {{ __('CATALOGOS') }}<b class="caret"></b>
            </p>
          </a>
          <div class="collapse hide" id="catalogo">
            <ul class="nav">
              <li>
                <a href="/">
                  <span class="sidebar-mini-icon fa fa-lightbulb-o" ></span>
                  <span class="sidebar-normal">{{ __('SUBMENU 1') }}</span>
                </a>
              </li>
              <li>
                <a href="/">
                  <span class="sidebar-mini-icon fa fa-lightbulb-o" ></span>
                  <span class="sidebar-normal">{{ __('SUBMENU 2') }}</span>
                </a>
              </li>
            </ul>
          </div>
        </li>-->
        <!-- <li style="margin-top: 185% !important">
          <a href="https://codgo.com.mx">
            <img src="{{ asset('paper/img/codgo.png') }}" height="auto" width="40%">
          </a>
        </li> -->
      </ul>
    </div>
</div>
