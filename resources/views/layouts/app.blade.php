<!--
=========================================================
 Paper Dashboard - v2.0.0
=========================================================

 Product Page: https://www.creative-tim.com/product/paper-dashboard
 Copyright 2019 Creative Tim (https://www.creative-tim.com)
 UPDIVISION (https://updivision.com)
 Licensed under MIT (https://github.com/creativetimofficial/paper-dashboard/blob/master/LICENSE)

 Coded by Creative Tim

=========================================================

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software. -->



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('paper') }}/img/apple-icon.png" />
    <link rel="icon" type="image/png" href="{{ asset('paper') }}/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        {{ __('Marlik') }}
    </title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
        name='viewport' />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.3.0/main.css" />



    {{-- <link href="https://demos.creative-tim.com/paper-dashboard/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="https://demos.creative-tim.com/paper-dashboard/assets/css/paper-dashboard.min.css?v=2.0.1" rel="stylesheet" />
    <link href="https://demos.creative-tim.com/paper-dashboard/assets/demo/demo.css" rel="stylesheet" /> --}}
    <link href="{{ asset('paper') }}/css/bootstrap.min.css" rel="stylesheet" />
    <link href="{{ asset('paper') }}/css/paper-dashboard.css?v=2.0.0" rel="stylesheet" />
    <link href="{{ asset('paper') }}/demo/demo.css" rel="stylesheet" />

    <style >
      i{
        font-size: 16px !important;
        letter-spacing: 4px;
      }
      button, a{
        margin-right: 5px !important;
      }
      .table td, .table th{
        padding: :10px !important;
        font-size: 10px !important;
      }

      /*#detalleContent{
        display: table !important;
      }*/
    </style>

</head>

<body class="{{ $class }}">
    <div id="fb-root"></div>
    {{-- @auth() --}}
        @include('layouts.page_templates.auth')
        {{-- @include('layouts.navbars.fixed-plugin') 
    @endauth

    @guest
        @include('layouts.page_templates.guest')
    @endguest --}}

    <script src="https://demos.creative-tim.com/paper-dashboard/assets/js/core/jquery.min.js"></script>
    <script src="https://demos.creative-tim.com/paper-dashboard/assets/js/core/popper.min.js"></script>
    <script src="https://demos.creative-tim.com/paper-dashboard/assets/js/core/bootstrap.min.js"></script>
    <script src="https://demos.creative-tim.com/paper-dashboard/assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
    <!-- Chart JS -->
    <script src="https://demos.creative-tim.com/paper-dashboard/assets/js/plugins/chartjs.min.js"></script>
    <!--  Notifications Plugin    -->
    <script src="https://demos.creative-tim.com/paper-dashboard/assets/js/plugins/bootstrap-notify.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.js"></script>
    <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="https://demos.creative-tim.com/paper-dashboard/assets/js/paper-dashboard.min.js?v=2.0.1" type="text/javascript"></script>
    <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
    <script src="https://demos.creative-tim.com/paper-dashboard/assets/demo/demo.js"></script> --}}

    <!--   Core JS Files   -->
    <script src="{{ asset('paper') }}/js/core/jquery.min.js"></script>
    <script src="{{ asset('paper') }}/js/core/popper.min.js"></script>
    <script src="{{ asset('paper') }}/js/core/bootstrap.min.js"></script>
    <script src="{{ asset('paper') }}/js/plugins/perfect-scrollbar.jquery.min.js"></script>
    <!-- Chart JS -->
    <script src="{{ asset('paper') }}/js/plugins/chartjs.min.js"></script>
    <!--  Notifications Plugin    -->
    <script src="{{ asset('paper') }}/js/plugins/bootstrap-notify.js"></script>
    <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
    <script src="{{ asset('paper') }}/js/paper-dashboard.min.js?v=2.0.0" type="text/javascript"></script>
    <!-- Paper Dashboard DEMO methods, don't include it in your project! -->
    <script src="{{ asset('paper') }}/demo/demo.js"></script>






    {{-- VUE JS --}}
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    {{-- SWEET ALERT  --}}
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    {{-- AXIOS --}}
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    {{-- VUE CURRENCY --}}
    <script src="https://unpkg.com/vue-currency-filter@3.2.3/dist/vue-currency-filter.iife.js"></script>
    {{-- CALENDARIO  --}}
    <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.3.0/main.min.js"></script>
    {{-- CHART-JS --}}
    <script src="https://www.jsdelivr.com/package/npm/chart.js?path=dist"></script>
    {{-- PROGRESS BAR --}}
    <script src="https://rawgit.com/kimmobrunfeldt/progressbar.js/1.0.0/dist/progressbar.js"></script>

    <script type="text/javascript">


			if (VueCurrencyFilter) {
					Vue.use(VueCurrencyFilter, {
						symbol: "$",
						thousandsSeparator: ",",
						fractionCount: 2,
						fractionSeparator: ".",
						symbolPosition: "front",
						symbolSpacing: false
					})
				}
    </script>

    @stack('scripts')

    @yield('vuescripts')

    @include('layouts.navbars.fixed-plugin-js')
</body>

</html>
