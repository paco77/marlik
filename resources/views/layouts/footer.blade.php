<footer class="footer footer-black  footer-white ">
    <div class="container-fluid">
        <div class="row">
            <nav class="footer-nav">
                <ul>
                    <li>
                        <a href="#" target="_blank">{{ __('FACEBOOK') }}</a>
                    </li>
                    <li>
                      <a href="#" target="_blank">{{ __('INSTAGRAM') }}</a>
                    </li>
                    <li>
                      <a href="#" target="_blank">{{ __('YOUTUBE') }}</a>
                    </li>
                </ul>
            </nav>
            <div class="credits ml-auto">
                <span class="copyright">
                    ©
                    <script>
                        document.write(new Date().getFullYear())
                    </script>
                </span>
            </div>
        </div>
    </div>
</footer>
