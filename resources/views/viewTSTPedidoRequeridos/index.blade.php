@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'user'
])

@section('content')
  <style media="screen">
    table th, td{
      text-align: center !important;
    }
  </style>
  <div class="content" id="app">
    @if (session('message'))
        <div class="alert alert-success" role="alert">
            {{ session('message') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger" role="alert">
            {{ session('error') }}
        </div>
    @endif

      <div class="card shadow col-md-12">
          <div class="card-header border-0">
            <div class="row align-items-center">
                <div class="col-md-10">
                    <h3 class="mb-0">PEDIDOS</h3><br>
                </div>
            </div>
          </div>
          <div class="table-responsive">
            <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Position</th>
                <th>Office</th>
                <th>Age</th>
                <th>Start date</th>
                <th>Salary</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Tiger Nixon</td>
                <td>System Architect</td>
                <td>Edinburgh</td>
                <td>61</td>
                <td>2011-04-25</td>
                <td>$320,800</td>
            </tr>
            <tr>
                <td>Garrett Winters</td>
                <td>Accountant</td>
                <td>Tokyo</td>
                <td>63</td>
                <td>2011-07-25</td>
                <td>$170,750</td>
            </tr>
            <tr>
                <td>Ashton Cox</td>
                <td>Junior Technical Author</td>
                <td>San Francisco</td>
                <td>66</td>
                <td>2009-01-12</td>
                <td>$86,000</td>
            </tr>
            <tr>
                <td>Cedric Kelly</td>
                <td>Senior Javascript Developer</td>
                <td>Edinburgh</td>
                <td>22</td>
                <td>2012-03-29</td>
                <td>$433,060</td>
            </tr>
            <tr>
                <td>Airi Satou</td>
                <td>Accountant</td>
                <td>Tokyo</td>
                <td>33</td>
                <td>2008-11-28</td>
                <td>$162,700</td>
            </tr>
            <tr>
                <td>Colleen Hurst</td>
                <td>Javascript Developer</td>
                <td>San Francisco</td>
                <td>39</td>
                <td>2009-09-15</td>
                <td>$205,500</td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <th>Name</th>
              <th>Position</th>
              <th>Office</th>
              <th>Age</th>
              <th>Start date</th>
              <th>Salary</th>
            </tr>
          </tfoot>
    </table>
          </div>
      </div>
  </div>
@endsection

@section('vuescripts')


  <script type="text/javascript">
  import $ from 'jquery'
    var app = new Vue({
      el: '#app',
      data: {
        buscar: '',
        articulos: [],
        arch:null
      },
      methods:{
        eliminar: function(e){
        },
        busqueda: function(){

        },
        getArticulos: function(){
          let url='articulosindex'

          /*axios.get(url).then(response=>{
            this.articulos=response.data.articulos
            console.log(this.articulos)
          })*/
        },
        onChange: function(e){
            let fd= new FormData();
            let url="importexcel"
            this.arch=e.target.files[0]
            console.log(this.arch)
            axios.post(url, fd).then(response=>{
                console.log(response.data)
                location.reload();
            })

        }
        
      },
      mounted: function () {
        let t = this;
        $('#example').DataTable({
          order: [[3, 'desc']],
        });
        this.$nextTick(function () {
          t.getArticulos();
        });
      }
    })
  </script>

@endsection