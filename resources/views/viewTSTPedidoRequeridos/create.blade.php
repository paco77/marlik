@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'empresas'
])

@section('content')
    <style media="screen">
        table td, th{
          text-align: center !important;
        }
        .notalign{
          text-align: left !important;
          padding-left: 120px !important
        }
    </style>
    <div class="content" id="vue-app">

      <div class="card shadow">
        @if($modo == 1)
          <form action="/articulos" method="post" enctype="multipart/form-data" @submit="validarFormulario">
        @else
          <form action="/articulos/{{$articulo->id}}" method="post" enctype="multipart/form-data" @submit="validarFormulario">
          @method('PUT')
        @endif
          @csrf
          <div class="card-header border-0">
              <div class="row align-items-center">
                  <div class="col-md-10">
                    @if($modo == 1)<h3 class="mb-0">REGISTRAR ARTICULO</h3> @else <h3 class="mb-0">EDITAR ARTICULO</h3> @endif
                    <br>
                  </div>
                  <div class="col-md-2 col-sm-12 text-right">
                    <a href="/user" class="btn btn-warning col-12 btn-round"><i class="fa fa-arrow-left"> </i> REGRESAR</a>
                  </div>
              </div>
              <hr>
              <div class="row card-body">
                <h5>Información general </h5>
                <div class="col-md-12">
                  <div class="row">
                      
                    <div class="col-md-3 col-sm-12 form-group">
                      <label for="">Codigo de Barras*</label>
                      <input type="text" class="form-control" name="codigo_de_barras" value="{!!$articulo->codigo_de_barras!!}" required>
                    </div>
                    <div class="col-md-3 col-sm-12 form-group">
                      <label for=""> Codigo *</label>
                      <input type="text" class="form-control" name="codigo" value="{!!$articulo->codigo!!}" required>
                    </div>
                    <div class="col-md-3 col-sm-12 form-group">
                      <label for="">Clave</label>
                      <input type="text" class="form-control" name="clave" value="{!!$articulo->clave!!}">
                    </div>
                    <div class="col-md-3 col-sm-12 form-group">
                      <label for="">minimo*</label>
                      <input type="text" class="form-control" name="minimo" value="{!!$articulo->minimo!!}" required>
                    </div>
                    <div class="col-md-12 col-sm-12 form-group">
                      <label for="">Descripcion</label>
                      <input type="text" class="form-control" name="descripcion" value="{!!$articulo->descripcion!!}">
                    </div>
                    <div class="col-md-3 col-sm-12 form-group">
                      <label for=""> linea *</label>
                      <input type="text" class="form-control" name="linea" value="{!!$articulo->linea!!}" required>
                    </div>
                    <div class="col-md-3 col-sm-12 form-group">
                      <label for="">marca</label>
                      <input type="text" class="form-control" name="marca" value="{!!$articulo->marca!!}">
                    </div>
                    <div class="col-md-3 col-sm-12 form-group">
                      <label for="">minim</label>
                      <input type="text" class="form-control" name="minim" value="{!!$articulo->minim!!}">
                    </div>
                    <div class="col-md-3 col-sm-12 form-group">
                      <label for="">Distrib</label>
                      <input type="text" class="form-control" name="ditrib" value="{!!$articulo->ditrib!!}" required>
                    </div>
                    <div class="col-md-3 col-sm-12 form-group">
                      <label for=""> Estrategico</label>
                      <input type="text" class="form-control" name="estrategico" value="{!!$articulo->estrategico!!}" required>
                    </div>
                    <div class="col-md-3 col-sm-12 form-group">
                      <label for="">Sub_sin_iva</label>
                      <input type="text" class="form-control" name="sub_sin_iva" value="{!!$articulo->sub_sin_iva!!}">
                    </div>
                    <div class="col-md-3 col-sm-12 form-group">
                      <label for="">Publico_sin_iva</label>
                      <input type="text" class="form-control" name="publico_sin_iva" value="{!!$articulo->publico_sin_iva!!}" required>
                    </div>
                    <div class="col-md-3 col-sm-12 form-group">
                      <label for="">Sat</label>
                      <input type="text" class="form-control" name="sat" value="{!!$articulo->sat!!}">
                    </div>
                    <div class="col-md-3 col-sm-12 form-group">
                      <label for=""> Unidad</label>
                      <input type="text" class="form-control" name="unidad" value="{!!$articulo->unidad!!}" required>
                    </div>
                  </div>
                </div>
               
              </div>
              <div class="card-footer">
                <div class="col-md-12 col-sm-12 text-right"><br><br>
                  <button type="submit" class="btn btn-success col-md-2 btn-round">Guardar</button>
                </div>
              </div>
          </div>
        </form>
      </div>
    </div>
@endsection

@section('vuescripts')
  <script type="text/javascript">
      var app = new Vue({
        el: '#vue-app',
        data: {
          c_empresas: false,
          a_empresas: false,
          b_empresas: false,
          m_empresas: false,
          c_usuarios: false,
          a_usuarios: false,
          b_usuarios: false,
          m_usuarios: false,
          c_objeciones: false,
          a_objeciones: false,
          b_objeciones: false,
          m_objeciones: false,
          c_recomendaciones: false,
          a_recomendaciones: false,
          b_recomendaciones: false,
          m_recomendaciones: false,
          password: '',
          password2: '',
        },
        methods:{
          existe: function(variable){
          },
          validarFormulario: function(e){

          }
        },
        mounted: function () {
          this.$nextTick(function () {
            let t = this;
        }
      });

  </script>
@endsection