@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'dashboard'
])
@section('content')
<div class="content" id="vue-app">
    <div class="card shadow col-md-12">

        <div id="alert1" class="alert alert-success alert-dismissible fade show">@{{mensaje}}</div>
        <h2>Nuevo Usuario</h2>
        <form method = 'POST' v-on:submit.prevent="guardar">
            <div class="form-group">
                <label for="exampleInputEmail1">Nombre *</label>
                <input type="text" class="form-control" id="name" v-model="name" aria-describedby="nameHelp" placeholder="Nombre" required>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Correo Electronico *</label>
                <input type="email" class="form-control" id="exampleInputEmail1" v-model="email" aria-describedby="emailHelp" placeholder="Enter email" required>
            
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Teléfono </label>
                <input type="text" class="form-control" id="telefono" v-model="phone" aria-describedby="phoneHelp" placeholder="Telefono">
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">Teléfono Celular</label>
                <input type="text" class="form-control" id="cellphone" v-model="cellphone" aria-describedby="phoneHelp" placeholder="Celular">
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1">Rol *</label>
                <select class="form-control" id="exampleFormControlSelect1" v-model="rol" required>
                  <option>Supervisor</option>
                  <option>Tecnico</option>
                  <option>Operador</option>
                </select>
            </div>

            <div class="form-group">
                <label for="exampleFormControlSelect1">Area *</label>
                <select class="form-control" id="exampleFormControlSelect1" v-model="area" required>
                    <option>VACIADO</option>
                    <option>TRANSFORMACION</option>
                    <option>CALIDAD INTER</option>
                    <option>MATIZADO</option>
                    <option>CALIDAD LIBERACION</option>
                    <option>PRODUCTO LIBERADO</option>
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password *</label>
                <input type="password" class="form-control" id="exampleInputPassword1" v-model="password" placeholder="Password" required>
            </div>
            <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
        
    </div>
</div>
@endsection

@push('scripts')
    <script>
        var app = new Vue({
        el: '#vue-app',
        data: {
            name:'',
            email:'',
            phone:'',
            cellphone:'',
            area:'',
            rol:'',
            password:'',
            buscar: '',
            mensaje:''
           /* inmobiliaria: {
                nombre: '',
                direccion : ''
            }*/
        },
        methods:{ 
            getData: function(id){
            },

            guardar: function(){
                axios.post('/usermarlik', {name:this.name, email:this.email, phone:this.phone, cellphone:this.cellphone, area:this.area, rol:this.rol, password:this.password,}).then(response=>{
                    if(response.data.message=="Usuario Guardado")
                        this.mensaje="Usuario Registrado"
                    else
                        this.mensaje="No se ha guardado el usuario"
                    $('#alert1').fadeTo(2000,500).slideUp(500, function(){
                        $('#alert1').slideUp(700)
                    })
                    this.name=''
                    this.email=''
                    this.phone=''
                    this.cellphone=''
                    this.area=''
                    this.rol=''
                    this.password=''
                    this.mensaje=''
                })
            }

        },

        mounted: function () {
        },
        created()
        {
            $('#alert1').hide()
        }
                
    })
    </script>
@endpush