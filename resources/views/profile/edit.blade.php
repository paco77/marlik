@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'profile'
])

@section('content')
  <style media="screen">
    .activate{
      background-color: #403d39 !important;
      color: white !important;
      border-color: #403d39 !important;
    }
    .card .card-body{
      padding: 0px !important
    }
    [v-cloak] { display: none; }
  </style>
    <div class="content" id="app">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        @if (session('password_status'))
            <div class="alert alert-success" role="alert">
                {{ session('password_status') }}
            </div>
        @endif
        <div class="row">
          <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="card card-user">
                    <div class="image">
                      <img src="{{ asset('paper/img/jan-sendereks.jpg') }}" alt="...">
                    </div>
                    <div class="card-body">
                        <div class="author">
                            <img class="avatar border-gray" style="background-color: #fff;" src="/storage/logos/{{__(auth()->user()->company->logo)}}" alt="logo">
                            <h5 class="title" style="color: #51bcda">{{ __(auth()->user()->name)}}</h5>
                            <p class="description" style="color: gray !important">
                              @ {{ __(auth()->user()->roles()->first()->name == 'EMPRESA' ? 'ADMINISTRADOR' : 'EMPLEADO')}}<br>
                                {{ __(auth()->user()->company->nombre)}}<br>
                                {{ __(auth()->user()->company->ubicacion)}}<br>
                                {{ __(auth()->user()->company->telefono)}}
                            </p>
                        </div>
                    </div>
                    <hr>
                    <div v-cloak class="card-footer" style="text-align:center">
                      <div>
                        <button type="button" :class="[activado == 2 ? 'activate': '' , 'btn btn-round btn-sm btn-link']" style="border: 2px solid #66615B" @click="activado = 2"><i class="fa fa-pencil"></i> EDITAR PERFIL </button>
                        <button {{ Auth::user()->roles()->first()->name != 'EMPRESA' ? 'disabled' : ''}} type="button" :class="[activado == 1 ? 'activate': '' , 'btn btn-round btn-sm btn-link']" style="border: 2px solid #66615B" @click="activado = 1"><i class="fa fa-pencil"></i> EDITAR EMPRESA</button>
                        <button type="button" :class="[activado == 0 ? 'activate': '' , 'btn btn-round btn-sm btn-link']" style="border: 2px solid #66615B" @click="activado = 0"><i class="fa fa-lock"></i> CAMBIAR CONTRASEÑA </button>
                      </div>
                      <br>
                      <div>
                        <button disabled type="button" :class="[activado == 3 ? 'activate': '' , 'btn btn-round btn-sm btn-link']" style="border: 2px solid #66615B" @click="activado = 3"><i class="fa fa-star"></i> MEMBRESIA </button>
                        <button disabled type="button" :class="[activado == 4 ? 'activate': '' , 'btn btn-round btn-sm btn-link']" style="border: 2px solid #66615B" @click="activado = 4"><i class="fa fa-credit-card"></i> HISTORIAL DE PAGOS </button>
                      </div>

                    </div>
                </div>
                <div style="text-align: center">
                  <form action="{{ route('profile.password') }}" method="POST" v-show="activado == 0">
                      @csrf
                      @method('PUT')
                      <div class="card">
                          <div class="card-header">
                              <h5 class="title">{{ __('CAMBIAR CONTRASEÑA') }}</h5>
                          </div>
                          <div class="card-body">
                              <div class="row">
                                  <label class="col-md-3 col-form-label">{{ __('Contraseña anterior') }}</label>
                                  <div class="col-md-9">
                                      <div class="form-group">
                                          <input type="password" name="old_password" class="form-control" required>
                                      </div>
                                      @if ($errors->has('old_password'))
                                          <span class="invalid-feedback" style="display: block;" role="alert">
                                              <strong>{{ $errors->first('old_password') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>
                              <div class="row">
                                  <label class="col-md-3 col-form-label">{{ __('Nueva contraseña') }}</label>
                                  <div class="col-md-9">
                                      <div class="form-group">
                                          <input type="password" name="password" class="form-control"  required>
                                      </div>
                                      @if ($errors->has('password'))
                                          <span class="invalid-feedback" style="display: block;" role="alert">
                                              <strong>{{ $errors->first('password') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>
                              <div class="row">
                                  <label class="col-md-3 col-form-label">{{ __('Confirmar contraseña') }}</label>
                                  <div class="col-md-9">
                                      <div class="form-group">
                                          <input type="password" name="password_confirmation" class="form-control"  required>
                                      </div>
                                      @if ($errors->has('password_confirmation'))
                                          <span class="invalid-feedback" style="display: block;" role="alert">
                                              <strong>{{ $errors->first('password_confirmation') }}</strong>
                                          </span>
                                      @endif
                                  </div>
                              </div>
                          </div>
                          <div class="card-footer ">
                              <div class="row">
                                  <div class="col-md-12 text-center">
                                      <button type="submit" class="btn btn-info btn-round">{{ __('Guardar cambios') }}</button>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </form>
                    @if(auth()->user()->roles()->first()->name == 'EMPRESA')
                  <form action="{{ route('profile.company') }}" method="POST" enctype="multipart/form-data" v-show="activado == 1">
                      @csrf
                      @method('PUT')
                      <div class="card">
                          <div class="card-header">
                              <h5 class="title">{{ __('EDITAR DATOS EMPRESA') }}</h5>
                          </div>
                          <div class="card-body">
                            <div class="row">
                              <label class="col-md-3 col-form-label">Nombre empresa</label>
                              <div class="col-md-9">
                                <div class="form-group">
                                  <input type="text" name="nombre_empresa" value="{{__(auth()->user()->company->nombre)}}" class="form-control" required>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-md-3 col-form-label">Ubicacion</label>
                              <div class="col-md-9">
                                <div class="form-group">
                                  <input type="text" value="{{__(auth()->user()->company->ubicacion)}}" name="ubicacion" class="form-control" required>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-md-3 col-form-label">Telefono</label>
                              <div class="col-md-9">
                                <div class="form-group">
                                  <input type="text" value="{{__(auth()->user()->company->telefono)}}" name="telefono_empresa" class="form-control" required>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-md-3 col-form-label">Giro</label>
                              <div class="col-md-9">
                                <div class="form-group">
                                  <select class="form-control" name="giro">
                                    <option value="Automotriz">Automotriz</option>
                                    <option value="Tecnologia">Tecnologia</option>
                                    <option value="Construccion">Construccion</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <label class="col-md-3 col-form-label">Logotipo</label>
                              <div class="col-md-9">
                                <div class="form-inline">
                                  <input type="file" name="logotipo" >
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="card-footer ">
                              <div class="row">
                                  <div class="col-md-12 text-center">
                                      <button type="submit" class="btn btn-info btn-round">{{ __('Guardar cambios') }}</button>
                                  </div>
                              </div>
                          </div>
                      </div>
                    </form>
                    @endif
                    <form action="{{ route('profile.update') }}" method="POST" enctype="multipart/form-data" v-show="activado == 2">
                        @csrf
                        @method('PUT')
                        <div class="card">
                            <div class="card-header">
                                <h5 class="title">{{ __('EDITAR PERFIL') }}</h5>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <label class="col-md-3 col-form-label">{{ __('Nombre') }}</label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" name="name" class="form-control"  value="{{ auth()->user()->name }}" required>
                                        </div>
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 col-form-label">{{ __('Correo electronico') }}</label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="email" name="email" class="form-control" value="{{ auth()->user()->email }}" required>
                                        </div>
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 col-form-label">{{ __('Telefono') }}</label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" name="phone" class="form-control"  value="{{ auth()->user()->phone }}" required>
                                        </div>
                                        @if ($errors->has('phone'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 col-form-label">{{ __('Celular') }}</label>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <input type="text" name="cellphone" class="form-control"  value="{{ auth()->user()->cellphone }}" required>
                                        </div>
                                        @if ($errors->has('cellphone'))
                                            <span class="invalid-feedback" style="display: block;" role="alert">
                                                <strong>{{ $errors->first('cellphone') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ">
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" class="btn btn-info btn-round">{{ __('Guardar cambios') }}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('vuescripts')
  <script type="text/javascript">
  var app = new Vue({
    el: '#app',
    data: {
      activado: 2,
    },
    methods:{

    },
    mounted: function () {
      let t = this;
      this.$nextTick(function () {

      });
    }
  })
  </script>
@endsection
