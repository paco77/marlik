@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'dashboard'
])

@section('content')
<div class="content" id="vue-app">
    <div class="card shadow col-md-12">

        <div class="card-header border-0">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <h3 class="mb-0">INMOBILIARIAS</h3><br>
                </div>
                <div class="input-group no-border col-md-3">
                    <input type="text" value="" class="form-control" placeholder="Buscar inmobiliaria..." v-model="buscar" @input="filtrar">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <i class="nc-icon nc-zoom-split"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 text-right">
                    <button type="button" class="btn btn-secondary col-md-12" data-toggle="modal" @click="reiniciarModal" data-target="#modalInmobiliaria">
                        nueva inmobiliaria
                    </button>
                </div>
            </div>
        </div>

        <div class=" mt-3">
            <table class="table align-items-center table-flush" id="tablaInmobiliarias">
                <thead class="thead-light">
                    <tr>
                        <th scope="col"><i class="fa fa-building"></i> Inmobiliaria</th>
                        <th scope="col"><i class="fa fa-map-marker"></i> Direccion</th>
                        <th scope="col"><i class="fa fa-phone"></i> Telefono</th>
                        <th scope="col"><i class="fa fa-at"></i> Correo electronico</th>
                        <th scope="col"><i class="fa fa-ellipsis-h"></i></th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(a, index) in inmobiliarias">
                        <td>@{{a.nombre}}</td>
                        <td>@{{a.direccion }}</td>
                        <td>@{{a.telefono }}</td>
                        <td>@{{a.correo}}</td>
                        <td>
                            <div class="btn-group" style="border: 2px solid #121935; border-radius: 10px !important">
                                <button class="btn btn-sm btn-link actions"   @click="agentesInmobiliaria(index)"  data-toggle="tooltip" data-placement="bottom" title="Administrar agentes">
                                    <i class="fa fa-users"></i>   
                                </button>
                                <button class="btn btn-sm  btn-link actions" @click="editar(index)"  data-toggle="tooltip" data-placement="bottom" title="Editar" >
                                    <i class="fa fa-pencil"></i>
                                </button>
                                <button v-if="a.id != 1" class="btn btn-sm btn-link actions" @click="eliminar(a.id)" data-toggle="tooltip" data-placement="bottom" title="Eliminar" >
                                    <i class="fa fa-trash"></i>   
                                </button>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="card-footer py-4">
            
        </div>
    </div>
    {{-- Modal agentes por inmobiliaria --}}
    <div class="modal fade" id="modalAgAs" tabindex="-1" aria-labelledby="modalAgAsLabel" aria-hidden="true">
        <div class="modal-dialog" style="min-width: 40%;">
            <div class="modal-content" >
                <div class="modal-header">
                    <h5 class="modal-title" id="modalAgAsLabel">Agentes de @{{inmobiliaria.nombre}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <table class="table table-flush" >
                            <thead class="thead-light">
                                <tr>
                                    <th>Folio</th>
                                    <th>Cliente</th>
                                    <th>IdDocumento</th>
                                    <th>Fecha</th>
                                    <th>Departamento</th>
                                    <th>Cobrado</th>
                                    <th>Descuento</th>
                                    <th>Saldo</th>
                                    <th>Titulo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="ag in inmobiliaria.agentes" >
                                    <td><span style="font-size: 12px" v-if="ag.estatus" class="badge badge-success">ACTIVO</span><span style="font-size: 14px" v-else class="badge badge-danger">INACTIVO</span> </td>
                                    <td>@{{</td>
                                    <td>@{{}}</td>
                                    <td>@{{}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                
                </div>
            </div>
        </div>
    </div>

    <!-- Modal nueva -->
    <div class="modal fade" id="modalInmobiliaria" tabindex="-1" aria-labelledby="modalInmobiliariaLabel" aria-hidden="true">
        <div class="modal-dialog" style="min-width: 40%;">
            <div class="modal-content" >
                <div class="modal-header">
                    <h5 class="modal-title" id="modalInmobiliariaLabel">Detalles</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row ">
                       
                    </div>   
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button">Guardar</button>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@push('scripts')
    <script>
        var app = new Vue({
        el: '#vue-app',
        data: {
            buscar: '',
           /* inmobiliaria: {
                nombre: '',
                direccion : ''
            }*/
        },
        methods:{ 
            getData: function(id){
            }

        },

        mounted: function () {
        }
                
    })
    </script>
@endpush