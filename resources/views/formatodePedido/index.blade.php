@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'dashboard'
])

@section('content')
<div class="content" id="vue-app">
    <div class="card shadow col-md-12">

        <div class="card-header border-0">
            <div class="row align-items-center">
                <div class="col-md-7">
                    <h3 class="mb-0">PEDIDOS</h3><br>
                </div>
                <!--<div class="input-group no-border col-md-3">
                    <input type="text" value="" class="form-control" placeholder="Buscar inmobiliaria..." v-model="buscar" @input="filtrar">
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <i class="nc-icon nc-zoom-split"></i>
                        </div>
                    </div>
                </div>-->
               <!-- <div class="col-md-2 text-right">
                    <button type="button" class="btn btn-secondary col-md-12" data-toggle="modal" data-target="#modalInmobiliaria">
                        nueva inmobiliaria
                    </button>
                </div>-->
            </div>
        </div>

        <div class=" mt-3">
                        <table class="table table-flush" >
                            <thead class="thead-light">
                                <tr>
                                    <th>Folio</th>
                                    <th>Clave</th>
                                    <th>Descripción</th>
                                    <th>Cantidad</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="(pedido, index) in pedidos">
                                    <td><span style="font-size: 12px" v-if="ag.estatus" class="badge badge-success">ACTIVO</span><span style="font-size: 14px" v-else class="badge badge-danger">INACTIVO</span> </td>
                                    <td>@{{pedio.Folio}}</td>
                                    <td>@{{pedio.ReceptorRazonSocial}}</td>
                                    <td>@{{pedio.ID}}</td>
                                    <td>@{{pedio.FechaCortaDocumento}}</td>
                                </tr>
                            </tbody>
                        </table>
        </div>
    </div>
    
    <div class="modal fade" id="modalAgAs" tabindex="-1" aria-labelledby="modalAgAsLabel" aria-hidden="true">
        <div class="modal-dialog" style="min-width: 40%;">
            <div class="modal-content" >
                <div class="modal-header">
                    <h5 class="modal-title" id="modalAgAsLabel">Agentes</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">

                    </div>
                </div>
                <div class="modal-footer">
                
                </div>
            </div>
        </div>
    </div>

    <!-- Modal nueva -->
    <div class="modal fade" id="modalInmobiliaria" tabindex="-1" aria-labelledby="modalInmobiliariaLabel" aria-hidden="true">
        <div class="modal-dialog" style="min-width: 40%;">
            <div class="modal-content" >
                <div class="modal-header">
                    <h5 class="modal-title" id="modalInmobiliariaLabel">Detalles</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row ">
                       
                    </div>   
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button">Guardar</button>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection

@push('scripts')
    <script>
        var app = new Vue({
        el: '#vue-app',
        data: {
            buscar: '',
            pedidos:[]
        },
        methods:{ 
            getData: function(){
                axios.get('/api/index').then(function(response){
                    this.pedidos=response.data.pedidos
                    console.log(response.data)
                })
            }

        },
        created: function(){
            //this.getData()
        },

        mounted: function(){
        let t = this;
        this.$nextTick(function () {
          t.getData();
        });
      }
                
    })
    </script>
@endpush