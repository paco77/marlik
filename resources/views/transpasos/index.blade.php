@extends('layouts.app', [
    'class' => '',
    'elementActive' => 'dashboard'
])

@section('content')
<div class="content" id="vue-app">
    <!--Menu-->
    <nav class="navbar navbar-expand-lg navbar-light bg-dark">
      <div class="container-fluid">
        
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
                <a class="btn" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                    <i class="nc-icon"><img src="{{ asset('paper/img/folder_arch.png') }}"></i>
                    <p>{{ __('Archivo') }}</p>
                </a> 
            </li>

            <li class="nav-item">
                <a href="#" class="btn" @click="mostrarT(2)">
                    <i class="nc-icon"><img src="{{ asset('paper/img/calidad.png') }}"></i>
                    <p>{{ __('Calidad') }}</p>
                </a>
            </li>
            <li class="nav-item">
              <!--<a class="nav-link active" aria-current="page" href="#" data-toggle="modal" data-target="#modalAgAs">-->
                    <a href="#" class="btn" @click="mostrarT(1)">
                    <i class="nc-icon"><img src="{{ asset('paper/img/ordp.png') }}"></i>
                    <p>{{ __('Ordenes de Producción') }}</p>
                    </a>
            </li>
            <li class="nav-item">
                <a href="#" class="btn" @click="mostrarT(0)">
                    <i class="nc-icon"><img src="{{ asset('paper/img/approved.png') }}"></i>
                    <p>{{ __('Traspasos') }}</p>
                </a>  
            </li>
            <li class="nav-item">
                <a href="#" class="btn" @click="mostrarT(0)">
                    <i class="nc-icon"><img src="{{ asset('paper/img/inventario.png') }}"></i>
                    <p>{{ __('Mov al Inventario') }}</p>
                </a>  
                <li class="nav-item">
                <a href="#" class="btn" @click="mostrarT(0)">
                    <i class="nc-icon"><img src="{{ asset('paper/img/grafico.png') }}"></i>
                    <p>{{ __('Reporte') }}</p>
                </a>  
            </li>
            </li>
          </ul>
        </div>
      </div>
    </nav>



        <!-- Modal Libro -->
    <div class="modal fade" id="modalLibro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Libro</h5>    
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-9">  
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Fecha:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="fecha" v-model="fecha" placeholder="fehca">
                                </div> 
                                
                            </div>  
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Supervisor:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="client" v-model="empresa" placeholder="cliente">
                                </div>
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Buscar:</label>
                                <div class="col-sm-3">
                                    <input class="form-control" list="dataSerial" id="exampleDataList2" placeholder="Supervisor..." v-on:input="seleccion">
                                    <datalist id="dataS">
                                      <option v-for="serial in seriales" v:bind:value="serial">@{{serial.SerialNumber}}</option>
                                    </datalist>
                                </div> 
                                
                            </div>
                            <div class="form-group row">
                                <label for="inputEmail3" class="col-sm-2 col-form-label">Técnico:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="tecnico" v-model="empresa" placeholder="tecnico">
                                </div>

                                <label for="inputEmail3" class="col-sm-2 col-form-label">Buscar:</label>
                                <div class="col-sm-3">
                                    <input class="form-control" list="dataSerial" id="exampleDataList3" placeholder="Técnico..." v-on:input="seleccion">
                                    <datalist id="dataT">
                                      <option v-for="serial in seriales" v:bind:value="serial">@{{serial.SerialNumber}}</option>
                                    </datalist>
                                </div> 
                            </div>
                            <div class="form-group row">
                                <label for="finaldate" class="col-sm-2 col-form-label">Observac...: </label>
                                <div class="col-sm-4">
                                  <textarea name="Observaciones" row="5"></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <button type="button" class="btn btn-primary" v-on:click="mostrarTabla(1)">
                                    <span aria-hidden="true">Agregar...</span>
                                </button> 
                            </div>
                            
                        </div>

                        <div class="col-sm-3">
                            <div class="row">
                                <a href="#" class="btn btn-info" @click="programarSeriales">
                                    <i class="nc-icon"><img src="{{ asset('paper/img/floppy-disk.png') }}"></i> Guardar
                                </a>
                            </div>
                            <div class="row">
                                <a href="#" class="btn btn-info" @click="programarSeriales">
                                    <i class="nc-icon"><img src="{{ asset('paper/img/floppy-disk.png') }}"></i> Imprimir
                                </a>
                            </div>
                        </div> 
                    </div>

                    <div class="form-group row">
                        <label for="finaldate" class="col-sm-5 col-form-label">Observac...: </label>
                        <div class="col-sm-7">
                          <textarea name="Observaciones" row="5"></textarea>
                        </div>
                    </div>
                    <div class="row" style="padding-bottom: 2rem;">
                        <div class=" table-responsive mt-3" style="overflow-y: scroll; overflow-x: scroll; height: fit-content; max-height: 70vh; margin-top: 100px;">
                            <table id="detalles" class="table table-bordered table-striped align-middle">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Serie</th> 
                                        <th>Clave</th>
                                        <th>Descripcion</th>
                                        <th>Color</th>
                                        <th>Peso</th>
                                        <th>Operador</th>
                                        <th>Falla</th>
                                        <th>Observaciones</th>
                                        <th>Prog</th>
                                        <th>Estado</th>
                                        <th>PedidoFolio</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



<!--OBSERVACIONES-->
        <div class="card shadow col-md-12" id="calidadInter">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h3 class="mb-0" >CALIDAD INTERMEDIA</h3>
                </div>
                <div class="form-group row" style="padding-top: 5px;">
                    <a href="#" class="btn btn-info" @click="libroModal">
                        <i class="nc-icon"><img src="{{ asset('paper/img/examen.png') }}"></i>Crear libro
                    </a>
                    </div>
            </div>
            <div class="row" style="padding-bottom: 2rem;">
                <div class=" table-responsive mt-3" style="overflow-y: scroll; overflow-x: scroll; height: fit-content; max-height: 70vh; margin-top: 100px;">
                    <table id="detalles" class="table table-bordered table-striped align-middle">
                        <thead class="thead-dark">
                            <tr>
                                <th>Folio</th> 
                                <th>Supervisor</th>
                                <th>Técnico</th>
                                <th>Observaciones</th>
                                <th>Fecha</th>
                                <th>Cancelada</th>
                                <th>Fecha Cancelación</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>


    <!-- -->
    <div class="card shadow col-md-12" id="transp">

        <div class="card-header border-0">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h3 class="mb-0" >TRASPASOS</h3>
                </div>
                <div class="col-md-6">
                    <h4 class="mb-0" style="float:right;">@{{fecha}}</h4><br>
                </div>
               <!-- <div class="col-md-2 text-right">
                    <button type="button" class="btn btn-secondary col-md-12" data-toggle="modal" data-target="#modalInmobiliaria">
                        nueva inmobiliaria
                    </button>
                </div>-->
            </div>
        </div>
        <div class="row" style="padding-bottom: 3rem;">
            <div class="col-md-3"></div>
            <div class="col-md-3">
                <div class="row"><label for="Transpasos" class="form-label">TRANSPASO</label></div>
                
                <div class="row form-floating">
                    <select class="form-select" id="estacion" v-model="estacion" disabled  @change="onChange">
                    <option value="" disabled selected>Elige Dto</option>
                    <option  v-for="tr in dataTrans" v:bind:value="tr">@{{tr}}</option>
                  </select></div>

            <div class="row">
                <label for="exampleDataList" class="form-label">PRODUCTO</label>
                <input class="form-control" list="dataSerial" id="exampleDataList1" placeholder="producto..." v-on:input="seleccion">
                <datalist id="dataSerial">
                  <option v-for="serial in seriales" v:bind:value="serial">@{{serial.SerialNumber}}</option>
                </datalist>
            </div>

                <!--<label for="Transpasos" class="form-label">TRANSPASO</label>
                <input class="form-control" list="dataTrans" id="exampleDataList" placeholder="estación...">
                <datalist id="dataTrans">
                  <option v-for="tr in dataTrans" v:bind:value="tr">@{{tr}}</option>
                </datalist>   --> 
            </div>
            <div class="col-md-3"></div>
        </div>
        <div class="row align-items-center" style="padding-bottom:3rem;">
            <div class="col-md-3"></div>
                <div class="col-md-3">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>NS:</td>
                                <td style="color:blue;" align="right">@{{producto.NS}}</td>
                            </tr>
                            <tr>
                                <td>Clave:</td>
                                <td style="color:blue;" align="right">@{{producto.Clave}}</td>
                            </tr>
                            <tr>
                                <td>Descripción:</td>
                                <td style="color:blue;" align="right">@{{producto.Descripcion}}</td>    
                            </tr>
                            <tr>
                                <td>Color:</td>
                                <td style="color:blue;" align="right">@{{producto.Color}}</td>
                            </tr>
                            <tr>
                                <td>Peso en Kg:</td>
                                <td style="color:blue;" align="right">@{{producto.Peso}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-3"></div>
            </div>
            <div id="alert1" class="alert alert-success alert-dismissible fade show">Se actualizó la ubicación del producto</div>
        
    </div>

        <!-- -------------------------------------------->
    <div class="card shadow col-md-12" id="ordp">

        <div class="card-header border-0">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h3 v-if="tprogra==1" class="modal-title" id="modalAgAsLabel">Programación</h3>
                    <h3 v-if="tprogra==0" class="modal-title" id="modalAgAsLabel">Ordenes Producción</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <button type="button" class="btn btn-primary" v-on:click="mostrarTabla(1)">
            <span aria-hidden="true">Programacion</span>
            </button>
            <button type="button" class="btn btn-primary" v-on:click="mostrarTabla(0)">
            <span aria-hidden="true">Ord. Producción</span>
            </button>
        </div>
        <div class="row" v-if="tproduct==1">
             <button type="button" class="btn btn-success" @click="programarSig">
                <span aria-hidden="true">Programar</span>
            </button>
        </div>

        <!--        Ordenes Produccion   -->
        <div class="row" style="padding-bottom: 2rem;">
            <div class=" table-responsive mt-3" style="overflow-y: scroll; overflow-x: scroll; height: fit-content; max-height: 70vh; margin-top: 100px;">
                <table id="ord_produccion" class="table table-bordered table-striped align-middle">
                    <thead class="thead-dark">
                        <tr>
                            <th><input type="checkbox" v-model="select_all" @click="select"></th>
                            <th>ProductoID</th> 
                            <th>Fecha</th>
                            <th>Clave</th>
                            <th>Can</th>
                            <th>Descripcion</th>
                            <th>Kg</th>
                            <th>PedidoId</th>
                            <th>OrdProd</th>
                            <th>Prog</th>
                            <th>Restantes</th>
                            <th>PedidoFolio</th>
                        </tr>
                    </thead>
                    <tbody><!------------------------>
                        <tr v-for="pedido in pedidos">
                            <td><input type="checkbox" :value="pedido.id" v-model="selected"></td>
                            <td>@{{pedido.ProductID}}</td>
                            <td>@{{pedido.DateDocument}}</td>
                            <td>@{{pedido.ProductKey}}</td>
                            <td>@{{pedido.Quantity}}</td>
                            <td>@{{pedido.Descripcion}}</td>
                            <td></td>
                            <td>@{{pedido.PedidoID}}</td>
                            <td>@{{pedido.ordprod}}</td>
                            <td>@{{pedido.CantProgramada}}</td>
                            <td>@{{pedido.FaltaEntrega}}</td>
                            <td>@{{pedido.FolioPedido}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class=" mt-3">
                <table id="programacion" class="table table-bordered table-striped align-middle">
                    <thead class="thead-dark">
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>Folio</th>
                            <th>DateDocument</th>
                            <th>DateFrom</th>
                            <th>DateTo</th>
                            <th>StatusDeliveryID</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="pedido in pedidosG">
                            <td><input type="checkbox" :value="pedido.PedidoID" @change="onCambio(pedido)"></td>
                            <td>@{{pedido.DocumentID}}</td>
                            <td>@{{pedido.folio}}</td>
                            <td>@{{pedido.DateDocument}}</td>
                            <td>@{{pedido.DateFrom}}</td>
                            <td>@{{pedido.DateTo}}</td>
                            <td>0</td>
                        </tr>
                    </tbody>
                </table>
            </div>
    </div>

    <!-- Modal nueva -->
    <div class="modal fade" id="modalProgramar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Prog Producción</h5>
            <div id="alert2" class="alert alert-danger alert-dismissible fade show">La cantidad excede el número de piezas</div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-sm-6">
                    <a href="#" class="" @click="programPro">
                        <i class="nc-icon"><img src="{{ asset('paper/img/floppy-disk.png') }}"></i>
                    </a>
                </div>
                <div class="col-sm-3">
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-6 col-form-label">Folio: </label>
                        <div class="col-sm-6" align="left">
                            <input type="text" class="form-control" id="folio" v-model="ultimoProg" placeholder="folio">
                        </div>
                    </div>
                </div>
                <div class="col-sm-3"></div>
            </div> 
            <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Fecha documento:</label>
                <div class="col-sm-3">
                  <input type="text" class="form-control" id="fehcaDoc" v-model="dateDoc" placeholder="fehca">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Inicia Programación :</label>
                <div class="col-sm-3">
                  <input type="date" class="form-control" id="fechahoy" v-model="dateFrom" placeholder="fehca">
                </div>
            </div>
            <div class="form-group row">
                <label for="finaldate" class="col-sm-2 col-form-label">Termina programación:</label>
                <div class="col-sm-3">
                  <input type="date" class="form-control" id="enddate" v-model="dateTo" placeholder="fehca">
                </div>
            </div>
            <div class="row">
                <div class="col col-md-6" id="navbarNav">
                    <a href="#" class="" @click="programPro">
                        <i class="nc-icon"><img src="{{ asset('paper/img/plus.png') }}"></i>
                    </a>
                    <a href="#" class="" @click="programPro">
                        <i class="nc-icon"><img src="{{ asset('paper/img/boton-menos.png') }}"></i>
                    </a>
                    <a href="#" class="" @click="programPro">
                        <i class="nc-icon"><img src="{{ asset('paper/img/delete.png') }}"></i>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class=" table-responsive mt-3" style="overflow-y: scroll; overflow-x: scroll; height: fit-content; max-height: 70vh; margin-top: 40px;">
                    <table id="ord_produccion" class="table table-bordered table-striped align-middle res">
                        <thead class="thead-dark">
                            <tr>
                                <th></th>
                                <th>ID</th>
                                <th>Clave</th>
                                <th>Can</th>
                                <th>Programar</th>
                                <th>Programados</th>
                                <th>Descripcion</th>
                                <th>PedidoId</th>
                                <th>Pedido</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="pedido in product_prog">
                                <td><input type="checkbox" :value="pedido.id" v-model="selectedProg"></td>
                                <td>@{{pedido.ordprod}}</td>
                                <td>@{{pedido.ProductKey}}</td>
                                <td>@{{pedido.Quantity}}</td>
                                <td><input type="number" class="form-control"  v-model="pedido.canAprog" aria-describedby="emailHelp" v-on:keyup.enter="actCant(pedido)"></td>
                                <td>@{{pedido.CantProgramada}}</td>
                                <td>@{{pedido.Descripcion}}</td>
                                <td>@{{pedido.PedidoID}}</td>
                                <td>@{{pedido.FolioPedido}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            </div>
          </div>
        </div>
      </div>

        <!-- Modal programar -->
    <div class="modal fade" id="progS" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Programación de producción</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
                <nav class="navbar navbar-expand-lg navbar-light bg-dark">
                    <div class="container-fluid">
        
                        <div class="collapse navbar-collapse" id="navbarNav">
                          <ul class="navbar-nav">
                            <li class="nav-item">
                                <a href="#" type="button" class="btn btn-success" @click="programPro">
                                    <i class="nc-icon"><img src="{{ asset('paper/img/disk.png') }}"></i><p>{{ __('Guardar') }}</p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="#" class="btn btn- btn-primary" @click="programPro">
                                    <i class="nc-icon"><img src="{{ asset('paper/img/list.png') }}"></i><p>{{ __('Ord Prod') }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="btn btn-info" @click="serialp">
                                    <i class="nc-icon"><img src="{{ asset('paper/img/document.png') }}"></i><p>{{ __('Serie') }}</p>
                                </a> 
                            </li>
                            <li class="nav-item">
                                <a href="#" class="btn btn-success" @click="programPro">
                                    <i class="nc-icon"><img src="{{ asset('paper/img/envelope-open-text.png') }}"></i><p>{{ __('Vaciado') }}</p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="btn btn-primary" @click="programPro">
                                    <i class="nc-icon"><img src="{{ asset('paper/img/print.png') }}"></i><p>{{ __('Imprimir') }}</p>
                                </a> 
                            </li>
                          </ul>
                        </div>
                    </div>
                </nav>
            </div>
            <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Fecha documento:</label>
                <div class="col-sm-3">
                  <input type="text" class="form-control" id="fehcaDoc" v-model="dateDoc" placeholder="fehca">
                </div>
            </div>
            <div class="form-group row">
                <label for="inputEmail3" class="col-sm-2 col-form-label">Inicia Programación :</label>
                <div class="col-sm-3">
                  <input type="date" class="form-control" id="fechahoy" v-model="dateFrom" placeholder="fehca">
                </div>
            </div>
            <div class="form-group row">
                <label for="finaldate" class="col-sm-2 col-form-label">Termina programación:</label>
                <div class="col-sm-3">
                  <input type="date" class="form-control" id="enddate" v-model="dateTo" placeholder="fehca">
                </div>
            </div>
            
            <div class="row">
                <div class="col col-md-6" id="navbarNav">
                    <a href="#" class="" @click="programPro">
                        <i class="nc-icon"><img src="{{ asset('paper/img/plus.png') }}"></i>
                    </a>
                    <a href="#" class="" @click="programPro">
                        <i class="nc-icon"><img src="{{ asset('paper/img/boton-menos.png') }}"></i>
                    </a>
                    <a href="#" class="" @click="programPro">
                        <i class="nc-icon"><img src="{{ asset('paper/img/delete.png') }}"></i>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class=" table-responsive mt-3" style="overflow-y: scroll; overflow-x: scroll; height: fit-content; max-height: 70vh; margin-top: 40px;">
                    <table id="ord_produccion" class="table table-bordered table-striped align-middle res">
                        <thead class="thead-dark">
                            <tr>
                                <th></th>
                                <th>ID</th>
                                <th>Clave</th>
                                <th>Can</th>
                                <th>Descripcion</th>
                                <th>PedidoId</th>
                                <th>Folio</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="pedido in pedidos_a_programar">
                                <td><input type="checkbox" :value="pedido.id" v-on:click="serialp(pedido)"></td>
                                <td>@{{pedido.id}}</td>
                                <td>@{{pedido.ProductKey}}</td>
                                <td>@{{pedido.CantProgramada}}</td>
                                <td>@{{pedido.Descripcion}}</td>
                                <td>@{{pedido.Pedido}}</td>
                                <td>@{{pedido.FolioPedido}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            </div>
          </div>
        </div>
    </div>

            <!-- Modal programar -->
    <div class="modal fade" id="progSerial" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Programacion de series</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
                <div class="col-sm-10">  
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Fecha:</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="fehcaDoc" v-model="dateDoc" placeholder="fehca">
                        </div>  
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Pedido:</label>
                        <label for="inputEmail3" class="col-sm-4 col-form-label">@{{pedFolio}}</label>
                        
                    </div>  
                    <div class="form-group row">
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Cliente:</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control" id="client" v-model="empresa" placeholder="cliente">
                        </div>
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Prefijo:</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" v-model="prefijo" id="pref" placeholder="prefijo">
                        </div> 
                        
                    </div>
                    <div class="form-group row">
                        <label for="finaldate" class="col-sm-2 col-form-label">Tot.etiquetas</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control" id="total" placeholder="0" >
                        </div>
                        <label for="inputEmail3" class="col-sm-2 col-form-label">Copias:</label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="pref" placeholder="copias">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="finaldate" class="col-sm-2 col-form-label">De:</label>
                        <div class="col-sm-3">
                          <input type="text" class="form-control" id="de" v-model="idSeriales" placeholder="folio">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="finaldate" class="col-sm-2 col-form-label">Hasta:</label>
                        <div class="col-sm-3">
                          <input type="text" class="form-control" id="hasta" placeholder="folio">
                        </div>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group row">
                        <a href="#" class="btn btn-info" @click="programarSeriales">
                        <i class="nc-icon"><img src="{{ asset('paper/img/floppy-disk.png') }}"></i>Crear
                    </a>
                    </div>
                    <div class="form-group row">
                        <a href="#" class="btn btn-primary" @click="programPro">
                        <i class="nc-icon"><img src="{{ asset('paper/img/folder.png') }}"></i>Imprimir
                    </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class=" table-responsive mt-3" style="overflow-y: scroll; overflow-x: scroll; height: fit-content; max-height: 70vh; margin-top: 40px;">
                        <table id="ord_produccion" class="table table-bordered table-striped align-middle res">
                            <thead class="thead-dark">
                                <tr>
                                    <th>Serie</th>
                                    <th>Clave</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="pedido in clavesSeriales">
                                    <td>@{{pedido.serial}}</td>
                                    <td>@{{pedido.ProductKey}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-4"></div>
            </div>
            </div>
          </div>
        </div>
    </div>

    </div>
</div>

</div>
@endsection

@push('scripts')
    <script>
        var app = new Vue({
        el: '#vue-app',
        data: {
            buscar: '',
            aux:0,
            fecha:'',
            idSeriales:0,
            dateDoc:new Date().toLocaleDateString(),
            dateTo:null,
            dateFrom:null,
            tprogra:1,
            tproduct:0,
            seriales:[],
            prefijo:"",
            ultimoProg:0,
            empresa:"",
            pedFolio:"",
            pedidos:[],
            pedidosG:[],
            programar:0,
            product_prog:[],
            clavesSeriales:[],
            estacion:"",
            producto:[],
            selected:[],
            selectedSerial:[],
            libro:[],
            Supervisores:[],
            tecnicos:[],
            selectedP:"",
            insumos:[],
            select_all:false,
            pedidos_a_programar:[],
            selectedProg:[],
            dataTrans:[
                'VACIADO',
                'TRANSFORMACION',
                'CALIDAD INTER',
                'MATIZADO',
                'CALIDAD LIBERACION',
                'PRODUCTO LIBERADO',
                'PRODUCTO TERMINADO',
            ],
            select_all:false,
        },
        methods:{ 
            getData: function(){
                let vue=this;

                vue.product_prog=[]
                vue.dateDoc=new Date().toLocaleString();
                vue.dateTo=null
                vue.dateFrom=null

                vue.selected=[]
                axios.get('/api/serial').then(function(response){
                    vue.seriales=response.data.seriales
                    //console.log(vue.seriales)
                })
                axios.get('/api/index').then(function(response){
                    vue.pedidos=response.data.pedido
                    vue.pedidosG=response.data.pedidosG
                    vue.ultimoProg=response.data.ultimoProg
                    vue.idSeriales=response.data.idSerial
                    response.data.usuarios.map(function(x){
                        if(x.rol=="Supervisor")
                            vue.Supervisores.push(x)
                        if(x.rol=="Tecnico")
                            vue.tecnicos.push(x)
                    })
                    console.log(vue.idSeriales)
                })

            },

            /*Seleccionar ordenes para produccion*/
            select(){
                this.selected = [];
                console.log(this.pedidos.length)
                if(!this.select_all){
                    this.aux=1

                    for(let i in this.pedidos)
                        this.selected.push(this.pedidos[i].productID)
                }
            },

            checarInsumos: function(){
                console.log(this.selected)
                let vue =this;
                axios.post('insumos',{ productos:vue.selected}).then(response=>{
                    vue.insumos=response.data.insumos
                    console.log(vue.insumos)
                })
                $('#modalInsumos').modal();
            },

            /*Seleccion serie para traspasos*/
            seleccion(e){
                console.log(e.target.value)
                let serie=e.target.value
                let producto=[]
                for(i in this.seriales)
                {
                    if(this.seriales[i].SerialNumber==serie)
                    { 
                        producto=
                        {
                            NS: this.seriales[i]. SerialNumber,
                            Clave: this.seriales[i].ProductKey,
                            Descripcion: this.seriales[i].ProductName,
                            Color:"black",
                            Peso:28,
                        }
                        $('#estacion').removeAttr('disabled');
                        break;
                    }
                }
                this.producto=producto
                
                 
            },
            onChange(){

                   $('#alert1').fadeTo(2000,500).slideUp(500, function(){
                        $('#alert1').slideUp(700)
                    })
            },

            mostrarT: function(op)
            {
                alert(op)
                if(op==1)
                {
                    $("#ordp").css('display',"block")
                    $("#transp").css('display', 'none')
                    $('#calidadInter').css('display', 'none')
                }
                else{
                    if(op==0)
                    {
                        $("#ordp").css('display',"none")
                        $("#transp").css('display', 'block')
                        $('#calidadInter').css('display', 'none')
                    }
                    else
                    {
                        $("#ordp").css('display',"none")
                        $("#transp").css('display', 'none')
                        $('#calidadInter').css('display', 'block')
                    }
                }
            },
            mostrarTabla: function(op)
            {
                if(op==1)
                {
                    this.tprogra=1
                    this.tproduct=0
                    $("#programacion").css('display', "block")
                    $("#ord_produccion").css('display', "none")
                }
                else
                {
                    this.tprogra=0
                    this.tproduct=1
                    $("#programacion").css('display', "none")
                    $("#ord_produccion").css('display', "block")
                }
            },
            onCambio: function(val){
                let vue=this
                let folio=val.folio
                this.dateDoc=new Date().toLocaleDateString()
                axios.get('prodct/series/'+folio).then(response=>{
                    console.log(response.data)
                    vue.pedidos_a_programar=response.data.productos
                })
                console.log(this.dateDoc)
                $("#progS").modal({backdrop: 'static', keyboard: false})
            },

            /*Selecciona los productos a programar*/
            select(){
                this.selected = [];
                console.log(this.pedidos.length)
                if(!this.select_all){
                    this.aux=1

                    for(let i in this.pedidos)
                    {
                        this.selected.push(this.pedidos[i].id)
                        this.product_prog.push(this.pedidos[i])
                    }
                    console.log(this.product_prog);
                }
            },

            /* Desplega el modal para crear la progracion de los productos*/
            programarSig: function()
            {
                this.product_prog=[]
                for(let i in this.pedidos)
                {
                    for(let j in this.selected)
                    {
                        if(this.pedidos[i].id==this.selected[j])
                            this.product_prog.push(this.pedidos[i])
                    }
                }
                console.log(this.dateDoc)
                $("#modalProgramar").modal({backdrop: 'static', keyboard: false})
                this.selected=[]
            },

            /* Actualiza el numero de productos para la progracion */
            actCant: function(producto){
                console.log(producto)
                for (let i in this.product_prog)
                {
                    if(producto.id==this.product_prog[i].id)
                    {
                        if(this.product_prog[i].FaltaEntrega>=producto.canAprog)
                        {
                            //alert("Holi")
                            this.product_prog[i].CantProgramada=parseInt(producto.canAprog)+parseInt(producto.CantProgramada)
                            this.product_prog[i].FaltaEntrega=this.product_prog[i].Quantity-this.product_prog[i].CantProgramada
                        }
                        else
                            $('#alert2').fadeTo(2000,500).slideUp(500, function(){
                        $('#alert2').slideUp(700)
                    })
                    }
                }
                this.programar=0
                console.log(this.product_prog)
            },

            /* Guarda la programacion de un pedido*/
            programPro: function(){
                let vue=this
                axios.post('api/programer',{progDetalle:this.product_prog, fecha_doc:this.dateDoc, ultimo:this.ultimoProg, fecha_from:this.dateFrom, fecha_term:this.dateTo}).
                then(function(response){
                    console.log(response.data)

                    vue.getData()
                    $("#modalProgramar").modal('hide')
                    axios.get('api/obtenerultimo').then(response=>{
                        this.ultimoProg=response.data.ultimoProg
                    })
                })
                //console.log(this.product_prog)
            },

            programProduccion: function(){
                console.log(this.clavesSeriales)
            },

            /*Crear seriales para el producto a produccion*/
            serialp: function(producto){
                let vue=this
               // alert(this.selectedSerial)
                alert(producto.CantProgramada)
                vue.empresa=producto.cliente
                vue.prefijo=producto.cliente[0]
                vue.pedFolio=producto.FolioPedido

                for(let i=0; i<producto.CantProgramada;i++)
                {
                    producto['serial']=i.toString()+producto.cliente[0]
                    console.log(producto['serial'])
                    let p=JSON.parse(JSON.stringify(producto))
                    vue.clavesSeriales.push(p)
                }
                console.log(this.clavesSeriales)
                $('#progSerial').modal({backdrop: 'static', keyboard: false})
            },
            programarSeriales: function(){
                console.log(this.clavesSeriales)
                console.log(this.empresa)
                console.log(this.pedFolio)
                axios.post('api/programarseriales',{seriales:this.clavesSeriales, empresa:this.empresa}).
                then(function(response){
                    console.log(response.data.message)
                })

            },
            libroModal: function(){
                $('#modalLibro').modal({backdrop: 'static', keyboard: false})
            }

        },

        created: function(){
            let vue=this;
            date=new Date();
            this.fecha=date.getDay()+' - '+date.getMonth()+' - '+date.getFullYear() 
            console.log(this.fecha)
            $('#alert1').hide();
            $('#alert2').hide();
            $("#programacion").css('display', "block")
            $("#ord_produccion").css('display', "none")
            $("#ordp").css('display', 'none')
            $("#calidadInter").css('display', 'none')
            

            vue.getData();
        },

       /* mounted: function(){
            let t = this;
            this.$nextTick(function () {
                t.getData();
        });
      }*/
                
    })
    </script>
@endpush
@push('style')


@endpush