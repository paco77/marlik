<?php

namespace App;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\User;

class Company extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function admin(){
      $users = User::where('company_id', $this->id)->get();
      foreach ($users as $u) {
        if($u->roles()->first()->name == 'EMPRESA')
          return $u;
      }
    }

    public function employees()
    {
        return $this->hasMany('App\User');
    }

    public function products()
    {
        return $this->hasMany('App\Product');
    }

}
