<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Libro extends Model
{
    protected $table ='SerialProduccion';
    public $timestamps=false;

    protected $fillable=[
            'id',
            'Fecha',
            'Supervisor',
            'Tecnico',
            'Observacion',
            'Cancelada',
    ];
}
