<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMarlik extends Model
{
    protected $table = "UserMarlik";
    public $timestamps=false;


    protected $fillable=[
        
            'name',
            'email',
            'phone',
            'cellphone',
            'area',
            'rol',
            'active',
            'password',
            'company_id',
    ]; 
}
