<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
  use SoftDeletes;

  protected $dates = ['deleted_at'];

  public function objections(){
    return $this->belongsToMany('App\Objection', 'product_objections', 'product_id');
  }

  public function comercial()
  {
    return $this->hasOne('App\Comercial');
  }

  public function segmentation()
  {
    return $this->hasOne('App\Segmentation');
  }

  public function company()
  {
      return $this->belongsTo('App\Company');
  }

}
