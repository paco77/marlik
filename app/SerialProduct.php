<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SerialProduct extends Model
{
    protected $table="vwLBSProductSerialNumber";
}
