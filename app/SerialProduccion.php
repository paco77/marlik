<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SerialProduccion extends Model
{
    protected $table ='SerialProduccion';
    public $timestamps=false;

    protected $fillable=[
            'id',
            'fecha',
            'folioPedido',
            'serial',
            'productKey',
            'cliente',
            'pedido',
            'productName',
            'modulo',
    ];
}
