<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrdenesProduccion extends Model
{
    protected $table = 'ViewTST_ListaOrdenesdeProduccion';


    public function detalleOrdenes()
    {
        return $this->hasMany(ViewTST_DetalleOrdenesProduccion::class, 'DocumentID', 'DocumentID');
    }

    public function toArray(){
        $data = parent::toArray();
        $data['pedidos'] = $this->detalleOrdenes;
        return $data;
    }

    public function ordenes()
    {
        $pedido=array();
        $cont=0;
        $obj=[
            'index'=>0,
            'productID'=>'',
            'folio'=>'',
            'orden'=>'',
            'clave'=>'',
            'descripcion'=>'',
            'cantidad'=>'',
            'empresa'=>''
            ];
        $programP=ProgramProduction::all();
        $pedidos=OrdenesProduccion::where('StatusName','Iniciada')->where('PeriodYear', '>=','2022')->get();
        $pedidos=json_decode($pedidos);
        foreach ($pedidos as $p) {
            foreach ($p->pedidos as $value) {
                $obj['index']=$cont++;
                $obj['productID']=$value->ProductID;
                $obj['orden']=$p->Orden;
                $obj['ordprod']=$p->DocumentID;
                $obj['pedidoID']=$p->PedidoID;
                $obj['clave']=$value->ProductKey;
                $obj['descripcion']=$value->Description;
                $obj['cantidad']=$value->QUANTITY;
                $obj['fecha']=$p->DateDocument;
                $obj['empresa']=$p->OfficialName;
                array_push($pedido, $obj);
                $obj=[];
            }
        }
        return array($pedido,$programP);  
    }

    public function clon()
    {
        $pedidos=OrdenesProduccion::where('StatusName','Iniciada')->where('PeriodYear', '>=','2022')->get();
        $pedidos=json_decode($pedidos);
        foreach ($pedidos as $p) {
            foreach ($p->pedidos as $value) {
                $ordenD=new OrderDetail();
                $ordenD->ordprod=$p->DocumentID;
                $ordenD->folio=$value->Folio;
                $ordenD->Quantity=intval($value->QUANTITY);
                $ordenD->ProductID=$value->ProductID;
                $ordenD->PedidoID=$p->PedidoID;
                $ordenD->Descripcion=$value->Description;
                $ordenD->ProductKey=$value->ProductKey;
                $ordenD->FolioPedido=$p->Orden;
                $ordenD->DateDocument=$p->DateDocument;
                $ordenD->cliente=$p->OfficialName;
            
                //$obj['index']=$cont++;
                //$obj['productID']=$value->ProductID;
                //$obj['orden']=$p->Orden;
                //$obj['ordprod']=$p->DocumentID;
                //$obj['pedidoID']=$p->PedidoID;
                //$obj['clave']=$value->ProductKey;
                //$obj['descripcion']=$value->Description;
                //$obj['cantidad']=$value->QUANTITY;
                //$obj['fecha']=$p->DateDocument;
                //$obj['empresa']=$p->OfficialName;
                //array_push($pedido, $obj);
               // $obj=[];
                $ordenD->save();
            }
        }
    }

}
