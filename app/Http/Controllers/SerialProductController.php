<?php

namespace App\Http\Controllers;

use App\SerialProduct;
use Illuminate\Http\Request;

class SerialProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('transpasos.index');

    }

    public function indexApp()
    {
        
        $serial=SerialProduct::where('DateDocument', '>','2022-04-04 12:06:20')->get();
        $seriales=json_decode($serial);
        return response()->json([
            'seriales'=>$seriales
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SerialProduct  $serialProduct
     * @return \Illuminate\Http\Response
     */
    public function show(SerialProduct $serialProduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SerialProduct  $serialProduct
     * @return \Illuminate\Http\Response
     */
    public function edit(SerialProduct $serialProduct)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SerialProduct  $serialProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SerialProduct $serialProduct)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SerialProduct  $serialProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(SerialProduct $serialProduct)
    {
        //
    }
}
