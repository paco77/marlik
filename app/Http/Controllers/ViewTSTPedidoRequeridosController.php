<?php

namespace App\Http\Controllers;

use App\ViewTST_PedidoRequeridos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ViewTSTPedidoRequeridosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // $pedido=DB::table('ViewTST_PedidoRequerido')->get();
       $pedido=ViewTST_PedidoRequeridos::all(); 
       return view('viewTSTPedidoRequeridos.index');
       dd("$pedido");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ViewTST_PedidoRequeridos  $viewTST_PedidoRequeridos
     * @return \Illuminate\Http\Response
     */
    public function show(ViewTST_PedidoRequeridos $viewTST_PedidoRequeridos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ViewTST_PedidoRequeridos  $viewTST_PedidoRequeridos
     * @return \Illuminate\Http\Response
     */
    public function edit(ViewTST_PedidoRequeridos $viewTST_PedidoRequeridos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ViewTST_PedidoRequeridos  $viewTST_PedidoRequeridos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ViewTST_PedidoRequeridos $viewTST_PedidoRequeridos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ViewTST_PedidoRequeridos  $viewTST_PedidoRequeridos
     * @return \Illuminate\Http\Response
     */
    public function destroy(ViewTST_PedidoRequeridos $viewTST_PedidoRequeridos)
    {
        //
    }
}
