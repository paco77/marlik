<?php

namespace App\Http\Controllers;

use App\SerialProduccion;
use Illuminate\Http\Request;

class SerialProduccionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SerialProduccion  $serialProduccion
     * @return \Illuminate\Http\Response
     */
    public function show(SerialProduccion $serialProduccion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SerialProduccion  $serialProduccion
     * @return \Illuminate\Http\Response
     */
    public function edit(SerialProduccion $serialProduccion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SerialProduccion  $serialProduccion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SerialProduccion $serialProduccion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SerialProduccion  $serialProduccion
     * @return \Illuminate\Http\Response
     */
    public function destroy(SerialProduccion $serialProduccion)
    {
        //
    }
}
