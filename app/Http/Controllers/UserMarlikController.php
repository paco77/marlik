<?php

namespace App\Http\Controllers;

use App\UserMarlik;
use Illuminate\Http\Request;

class UserMarlikController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('userMarlik.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $user=new UserMarlik();
       
       $user->name=$request->name;
       $user->email=$request->phone;
       $user->cellphone= $request->cellphone;
       $user->area=$request->area;
       if($request->rol=="Supervisor")
            $user->rol=1;
        if($request->rol=="Tecnico")
            $user->rol=2;
        if($request->rol=="Operador")
        $user->rol=3;
       $user->password=bcrypt($request->password);
       $user->save();

        return response()->json([
            'message'=>"Usuario Guardado"
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UserMarlik  $userMarlik
     * @return \Illuminate\Http\Response
     */
    public function show(UserMarlik $userMarlik)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UserMarlik  $userMarlik
     * @return \Illuminate\Http\Response
     */
    public function edit(UserMarlik $userMarlik)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UserMarlik  $userMarlik
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserMarlik $userMarlik)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UserMarlik  $userMarlik
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserMarlik $userMarlik)
    {
        //
    }
}
