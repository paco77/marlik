<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;


class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param  \App\User  $model
     * @return \Illuminate\View\View
     */
    public function index(User $model)
    {
        return view('users.index', ['users' => $model->paginate(15)]);
    }

    public function create(){
      $user = new User();
      $modo = 1;
      return view('users.create', compact('user', 'modo'));
    }

    public function edit(Request $request, $id){
      $user = User::findOrfail($id);
      $modo = 2;
      return view('users.create', compact('user', 'modo'));
    }

    public function store(Request $request){
      $user = User::where('email', $request->email)->get()->first();
      if($user){
        \Session::flash('error', 'Lo sentimos! El usuario ya existe en la base de datos.');
        return redirect()->back();
      }
      $user = new User();
      $user->name   =  $request->nombre_usuario;
      $user->email  =   $request->email;
      $user->phone  =   $request->telefono_usuario;
      $user->cellphone  =   $request->celular;
      $user->active = true;
      $user->password = bcrypt($request->password);
      $user->save();
      $user->assignRole('ADMINISTRADOR');
      $user->syncPermissions($request['permisos']);
      \Session::flash('message', 'Usuario registrado correctamente');
      return redirect('user');
    }

    public function update(Request $request, $id)
    {
      $user = User::findOrfail($id);
      $user->name   =  $request->nombre_usuario;
      $user->email  =   $request->email;
      $user->phone  =   $request->telefono_usuario;
      $user->cellphone  =   $request->celular;
      if(!is_null($request->password)){
        $user->password = bcrypt($request->password);
      }
      $user->syncPermissions($request['permisos']);
      $user->save();
      \Session::flash('message', 'Usuario modificado correctamente.');
      return redirect('user');

    }

    public function destroy($id)
    {
      $user = User::findOrfail($id);
      $user->delete();
      \Session::flash('message', "Usuario eliminado correctamente");
      return redirect('user');
    }



}
