<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileRequest;
use App\Http\Requests\PasswordRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Company;

class ProfileController extends Controller
{
    /**
     * Show the form for editing the profile.
     *
     * @return \Illuminate\View\View
     */
    public function edit()
    {
        return view('profile.edit');
    }

    /**
     * Update the profile
     *
     * @param  \App\Http\Requests\ProfileRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ProfileRequest $request)
    {
        auth()->user()->update($request->all());

        return back()->withStatus(__('Perfil actualizado correctamente.'));
    }

    /**
     * Change the password
     *
     * @param  \App\Http\Requests\PasswordRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function password(PasswordRequest $request)
    {
        auth()->user()->update(['password' => Hash::make($request->get('password'))]);

        return back()->withPasswordStatus(__('Contrasena actualizada correctamente.'));
    }

    public function company(Request $request){
      $company = Company::findOrfail(\Auth::user()->company->id);
      $company->nombre = $request->nombre_empresa;
      $company->ubicacion = $request->ubicacion;
      $company->telefono = $request->telefono_empresa;
      $company->giro = $request->giro;
      if(!is_null($request['logotipo']))
      {
        Storage::disk('public')->delete("logos/". $company->logo);
        $file = $request->file('logotipo');
        $filename= $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $generated_name = uniqid() . time(). '.' . $extension;
        \Storage::disk('public')->put("logos/". $generated_name,  \File::get($file));
        $company->logo = $generated_name;
      }
      $company->save();

      return back()->withStatus(__('Datos de la empresa actualizados correctamente.'));
    }
}
