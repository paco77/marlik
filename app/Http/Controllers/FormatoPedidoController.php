<?php

namespace App\Http\Controllers;

use App\FormatoPedido;
use Illuminate\Http\Request;

class FormatoPedidoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('formatodePedido.index');
       // dd($pedido);
    }

    public function apiIndex()
    {
        $pedido=FormatoPedido::all()->sortByDesc('FechaCortaDocumento')->take(100)->toArray();
        return response()->json([
            'pedidos'=> $pedido,
            'success'=> true
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FormatoPedido  $formatoPedido
     * @return \Illuminate\Http\Response
     */
    public function show(FormatoPedido $formatoPedido)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FormatoPedido  $formatoPedido
     * @return \Illuminate\Http\Response
     */
    public function edit(FormatoPedido $formatoPedido)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FormatoPedido  $formatoPedido
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FormatoPedido $formatoPedido)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FormatoPedido  $formatoPedido
     * @return \Illuminate\Http\Response
     */
    public function destroy(FormatoPedido $formatoPedido)
    {
        //
    }
}
