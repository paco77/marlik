<?php

namespace App\Http\Controllers;

use App\OrdenesProduccion;
use App\ProgramProduction;
use App\UserMarlik;
use App\DetalleProgram;
use App\SerialProduccion;
use App\OrderDetail;
use App\ViewTST_HerramientaCompras;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

/*Pedido, clave fecha
descargar en excel
* 
*
*/

class OrdenesProduccionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('ordenesProduccion.index');
    }

    public function apiIndex()
    {
        $pedido=array(); 
        $documentID=0;
        $idSerial=0;
        $pedidosG=array();
        $orden= new OrdenesProduccion();
        $p=$orden->ordenes();
        $seriales=SerialProduccion::orderBy('id')->first();
        $usuarios=UserMarlik::where('active',1)->get();
        if(!is_null($seriales))
            $idSerial=$seriales->id;
        
        $ultimoProg=ProgramProduction::orderBy('DocumentID','desc')->first();
        if(is_null($ultimoProg))
            $documentID=1;
        else
            $documentID=$ultimoProg->DocumentID+1;
        $pedido=OrderDetail::all();
        $pedidosG=$p[1];

        return response()->json([
            'pedido'    => $pedido,
            'pedidosG'  => $pedidosG,
            'ultimoProg'=> $documentID,
            'idSerial'  => $idSerial,
            'usuarios'  => $usuarios
        ]);
    }

    public function ultimoProgramado()
    {
        $ultimoProg=ProgramProduction::orderBy('DocumentID','desc')->first();
        $documentID=$ultimoProg->DocumentID; 
        $documentID+=1;
        return response()->json([
            'ultimoProg'=>$documentID
        ]);  
    }

    public function programacion(Request $request)
    { 
        $pProduction= new ProgramProduction();
        $date=new Carbon();
        $productos=$request->progDetalle;

         $pProduction->DocumentID=$request->ultimo;
        $pProduction->DateDocument=date('Y-m-d');
        $pProduction->DateFrom=$request->fecha_from;
        $pProduction->DateTo=$request->fecha_term;
        $pProduction->DocumentTypeID=47;
        $pProduction->DateDocDelivery=$request->fecha_term;
        $pProduction->StatusExtraID=2;
        $pProduction->Deleted=0;
        $pProduction->Canceled=0;
        
        $pProduction->save();
        foreach ($productos as $producto) {
            $pDetalle=new DetalleProgram();
            $pDetalle->Programacion = $pProduction->id;
            $pDetalle->Pedido=$producto['PedidoID'];
            $pDetalle->ProductID=$producto['ProductID'];
           // $op=OrdenesProduccion::where('Pedido');
            $pDetalle->Descripcion=$producto['Descripcion'];
            $pDetalle->ProductKey=$producto['ProductKey'];
            $pDetalle->cliente=$producto['cliente'];
            $pDetalle->CantProgramada=intval($producto['CantProgramada']);
            $pDetalle->Entregados=0;
            $pDetalle->FaltaEntrega=intval($producto['CantProgramada']);
            $pDetalle->Dañados=0;
            $pDetalle->FolioPedido=$producto['FolioPedido'];
            $pDetalle->DateDocument=date('Y-m-d');
            $pDetalle->save();
            $od=OrderDetail::where('PedidoID', $pDetalle->Pedido)->where('ProductID',$pDetalle->ProductID)->first();
            
            $od->CantProgramada+=$pDetalle->CantProgramada;
            $od->FaltaEntrega=$od->Quantity-$od->CantProgramada;
            $od->save();
        }

        return response()->json([
            'success'=>true
        ]);
    }

    public function programarSeriales(Request $request)
    {
        $serialP=$request->seriales;
        $cliente=$request->empresa;

        foreach ($serialP as $value) {
            $sp=new SerialProduccion();
            $sp->fecha=date('Y-m-d');
            $sp->folioPedido=$value['FolioPedido'];
            $sp->serial=$value['serial'];
            $sp->ProductKey=$value['ProductKey'];
            $sp->cliente=$cliente;
            $sp->pedido=$value['Pedido'];
            $sp->productName=$value['cliente'];
            $sp->modulo="Almacen";
            $sp->save();
        }

        return response()->json([
            'success'=>true,
            'message'=>"Guardado"
        ]);
    }

    public function detallePedidos($prog)
    {
        $detalles=DetalleProgram::where('Programacion', $prog)->get();

        return response()->json([
            'productos'=>$detalles
        ]);
    }

    public function llenar()
    {
        $orden= new OrdenesProduccion();

        $orden->clon();
        return response()->json([
            'success'=>true
        ]);
    }

    public function insumoIndex(Request $request)
    {
        $insumos=ViewTST_HerramientaCompras::all();
        return response()->json([
            'insumos'=>$insumos
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrdenesProduccion  $ordenesProduccion
     * @return \Illuminate\Http\Response
     */
    public function show(OrdenesProduccion $ordenesProduccion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrdenesProduccion  $ordenesProduccion
     * @return \Illuminate\Http\Response
     */
    public function edit(OrdenesProduccion $ordenesProduccion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrdenesProduccion  $ordenesProduccion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrdenesProduccion $ordenesProduccion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrdenesProduccion  $ordenesProduccion
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrdenesProduccion $ordenesProduccion)
    {
        //
    }
}
