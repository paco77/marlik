<?php

namespace App\Http\Controllers;

if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
}

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Amranidev\Ajaxis\Ajaxis;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

use App\Company;
use App\User;
use App\Product;

class APIController extends Controller
{
  public function getEmployees(Request $request, $company_id){
    $employees = User::where('company_id', $company_id)->get()->toArray();
    return response()->json([
      'employees' => $employees,
      'success' => true
    ], 200);
  }
  public function getCompanies(Request $request){
    $companies = Company::all()->toArray();
    return response()->json([
      'companies' => $companies,
      'success' => true
    ], 200);
  }
  public function getAdminUsers(Request $request){
    $users = User::where('company_id', null)->get()->toArray();
    return response()->json([
      'users' => $users,
      'success' => true
    ], 200);
  }
  public function getProducts(Request $request, $company_id){
    $products = Product::where('company_id', $company_id)->get()->toArray();
    return response()->json([
      'products' => $products,
      'success' => true
    ], 200);
  }

  public function getProductsSearch($company_id, $busqueda){
    $products = Product::query()->whereLike(['nombre', 'descripcion', 'tipo'], $busqueda)->where('company_id', $company_id)->get()->toArray();
    return response()->json([
      'products' => $products,
      'success' => true
    ], 200);
  }
  public function getEmployeesSearch($company_id, $busqueda){
    $employees = User::query()->whereLike(['name', 'email', 'phone', 'cellphone'], $busqueda)->where('company_id', $company_id)->get()->toArray();
    return response()->json([
      'employees' => $employees,
      'success' => true
    ], 200);
  }
  public function getCompaniesSearch($busqueda){
    $companies = Company::query()->whereLike(['nombre', 'ubicacion', 'telefono', 'giro'], $busqueda)->get()->toArray();
    return response()->json([
      'companies' => $companies,
      'success' => true
    ], 200);
  }

}
