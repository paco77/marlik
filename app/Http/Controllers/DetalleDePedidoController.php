<?php

namespace App\Http\Controllers;

use App\DetalleDePedido;
use Illuminate\Http\Request;

class DetalleDePedidoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $detalle=DetalleDePedido::where('DepotName', 'Almacén')->get();
        dd($detalle);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DetalleDePedido  $detalleDePedido
     * @return \Illuminate\Http\Response
     */
    public function show(DetalleDePedido $detalleDePedido)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DetalleDePedido  $detalleDePedido
     * @return \Illuminate\Http\Response
     */
    public function edit(DetalleDePedido $detalleDePedido)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DetalleDePedido  $detalleDePedido
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DetalleDePedido $detalleDePedido)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DetalleDePedido  $detalleDePedido
     * @return \Illuminate\Http\Response
     */
    public function destroy(DetalleDePedido $detalleDePedido)
    {
        //
    }
}
