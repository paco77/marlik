<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgramProduction extends Model
{
    protected $table= 'ProductProgram';



    protected $fillable=[
        'DocumentID',
        'folio',
        'DateDocument',
        'DateFrom',
        'DateTo',
        'DocumentTypeID',
        'DateDocDelivery',
        'StatusExtraID',
        'Deleted',
        'Canceled',
    ];

    public $timestamps=false;

    public function detalle(){
        return $this->hasMany(DetalleProgram::class, 'folio','Programacion');
    }
}
