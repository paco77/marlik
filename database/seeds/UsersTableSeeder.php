<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = 'ADMINISTRADOR';
        $user->email = 'admin@marketingapp.com';
        $user->email_verified_at = now();
        $user->password = bcrypt('admin2020');
        $user->save();
        $user->assignRole('ADMINISTRADOR');
    }
}
