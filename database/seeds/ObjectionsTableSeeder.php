<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Objection;

class ObjectionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $o = new Objection();
        $o->descripcion = 'El precio es muy elevado';
        $o->save();
    }
}
