<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductProgram extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        {
        Schema::create('ProductProgram', function(Blueprint $table){
            $table->bigIncrements('folio');
            $table->date('DateDocument');
            $table->date('DateFrom');
            $table->date('DateTo');
            $table->integer('DocumentTypeID');
            $table->date('DateDocDelivery');
            $table->integer('StatusExtraId');
            $table->integer('Deleted');
            $table->integer('Canceled');
        });
    }    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('ProductProgram');    
     }
}
