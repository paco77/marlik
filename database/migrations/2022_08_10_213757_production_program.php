<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductionProgram extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ProductionProgram', function(Blueprint $table){
            $table->bigIncrements('folio');
            $table->string('DocumentID');
            $table->date('DateDocument');
            $table->date('DateFrom');
            $table->date('DateTo');
            $table->integer('DocumentTypeID');
            $table->date('DateDocDelivery');
            $table->integer('StatusExtraId');
            $table->integer('Deleted');
            $table->integer('Canceled');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ProductionProgram');
    }
}
