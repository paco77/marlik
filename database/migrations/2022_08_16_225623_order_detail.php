<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OrderDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        {
        Schema::create('OrderDetail', function(Blueprint $table){
            $table->id();
            $table->integer('ordprod');
            $table->integer('Folio');
            $table->integer('Cancelado')->default('0');
            $table->integer('Suprimido')->default('0');
            $table->integer('Quantity');
            $table->integer('ProductID');
            $table->integer('PedidoID');
            $table->string('Descripcion');
            $table->string('ProductKey');
            $table->integer('CantProgramada')->default('0');
            $table->integer('Entregados')->default('0');
            $table->integer('FaltaEntrega')->default('0');
            $table->integer('Dañados')->default('0');
             $table->string('cliente')->nullable();
            $table->string('FolioPedido');
            $table->date('DateDocument');
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
