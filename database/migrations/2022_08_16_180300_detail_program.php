<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DetailProgram extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DetailProgram', function(Blueprint $table){
            $table->id();
            $table->integer('Programacion');
            $table->integer('Pedido');
            $table->integer('ProductID');
            $table->string('Descripcion');
            $table->string('ProductKey');
            $table->integer('CantProgramada');
            $table->integer('Entregados');
            $table->integer('FaltaEntrega');
            $table->integer('Dañados');
             $table->string('cliente')->nullable();
            $table->string('FolioPedido');
            $table->date('DateDocument');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('DetailProgram');
    }
}
