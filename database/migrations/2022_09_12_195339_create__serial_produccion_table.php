<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSerialProduccionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('SerialProduccion', function (Blueprint $table) {
            $table->id();
            $table->string('fecha');
            $table->string('folioPedido');
            $table->string('serial');
            $table->string('productKey');
            $table->string('cliente');
            $table->string('pedido');
            $table->string('productName');
            $table->string('modulo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('SerialProduccion');
    }
}
